# 开始使用

> `GeneAdmin`是基于[layui](http://www.layui.com)定制的管理端UI框架，遵循layui的模块规范，如果您已经了解过layui的使用，在使用GeneAdmin的时候门槛极低，拿来即用，项目主页[http://gene.jianggujin.com/index.html](http://gene.jianggujin.com/index.html)。

## 第一部分 兼容性和面向场景

> GeneAdmin 兼容人类正在使用的全部浏览器（IE6/7除外），可作为 PC 端后台系统的速成开发方案。

## 第二部分 GeneAdmin结构

> 您可以通过[码云](https://gitee.com/jianggujin/GeneAdmin)获取GeneAdmin完整开发包，相关文件目录说明如下：

```
  ├─res //核心资源目录
  │  │─css
  │  │─img
  │  └─lib
  │     ├─layui //layui目录
  │     │─layui-modules  //基于layui的GeneAdmin模块目录
  │     └─......  //其他
```

## 第三部分 快速上手

获得GeneAdmin后，将其完整地部署到你的项目目录（或静态资源服务器），你只需要引入下述个文件（和原生layui一样）：

```
res/lib/layui/css/layui.css
res/lib/layui/layui.js
```

下面是一个使用GeneAdmin的基本页面格式

```html
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <link href="res/lib/layui/css/layui.css" rel="stylesheet" />
    </head>

    <body>
        <div gene-admin></div>
        <script type="text/javascript" src='res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: 'res/lib/layui-modules/js/' //必须设置
            }).use(['admin'], function() {
                var admin = layui.admin.render({
                    topnav: {},
                    navbar: {},
                    tab: {},
                    footer: 'footer'
                });
            });
        </script>
    </body>
</html>
```

> 需要注意的是，**layui的base配置必须进行配置**，否则GeneAdmin的模块将不生效，使用admin模块会自动包含顶部导航(topnav)、左侧导航(navbar)、选项卡(tab)，如果不对其参数进行配置，则对应模块不会初始化。

GeneAdmin加载资源的时候需要正确的上下文根，否则可能会加载资源失败，如果您想更改上下文根，只需要在引入js文件之前增加`_context`变量指定新的值就可以了，详情请查看`环境配置`。

```html
<script type="text/javascript">var _context = '/GeneAdmin';</script>
<script type="text/javascript" src='res/lib/layui/layui.js'></script>
```

