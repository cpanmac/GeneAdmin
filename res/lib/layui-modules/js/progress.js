/**
 * Name:progress.js
 * Author:gene.jiang
 * E-mail:1013195469@qq.com
 * Website:http://www.jianggujin.com/
 * LICENSE:Apache v2 License
 */
layui.define(['jquery', 'element', 'tool'], function(exports) {
    var $ = jQuery = layui.jquery,
        _tool_ = layui.tool,
        _body_ = $('body'),
        _element_ = layui.element;
    var Progress = function(options) {
        this.__render(options);
    };
    Progress.fn = Progress.prototype;
    Progress.fn.getFilter = function() {
        return this._filter;
    };
    Progress.fn.__render = function(options) {
        if(this._initialized) {
            layui.hint().error('Progress已经初始化！');
            return;
        }
        options = options || {};

        //判断是否配置导航容器元素
        if(!!options.elem) {
            this._container = $(options.elem);
            //存在配置的容器
            if(!this._container.length) {
                this._container = undefined;
            }
        }
        if(!this._container) {
            //选取默认容器
            this._container = _body_.find('div[gene-progress]');
        }

        if(!this._container || !this._container.length) {
            layui.hint().error('Progress没有正常初始化！');
            return;
        }
        this._container = this._container.eq(0);
        this._filter = options.filter || _tool_.makeHashCode('geneProgress');

        var _that = this,
            _elem = this._container,
            _bar = $('<div class="layui-progress-bar"></div>');
        _elem.empty();
        _elem.addClass('layui-progress');
        _elem.attr('lay-filter', _that._filter);
        if(!!options.showPercent) {
            _elem.attr('lay-showPercent', 'yes');
        }
        if(!!options.big) {
            _elem.addClass('layui-progress-big');
        }
        if(!!options.percent) {
            _bar.attr('lay-percent', options.percent);
        }
        if(!!options.bg) {
            _bar.addClass('layui-bg-' + options.bg);
        }
        _elem.append(_bar);

        _element_.init('progress', _that._filter);
        _that._initialized = true;
        return _that;
    };

    Progress.fn.progress = function(value) {
        _element_.progress(this._filter, value);
    };

    function ProgressExport() {}
    ProgressExport.prototype.render = function(options) {
        return new Progress(options);
    };
    exports('progress', new ProgressExport());
});