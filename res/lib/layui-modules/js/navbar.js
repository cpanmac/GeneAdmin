/**
 * Name:navbar.js
 * Author:gene.jiang
 * E-mail:1013195469@qq.com
 * Website:http://www.jianggujin.com/
 * LICENSE:Apache v2 License
 * Description:导航栏
 */
layui.define(['layer', 'element', 'tool'], function(exports) {
    var $ = layui.jquery,
        _body_ = $('body'),
        _tool_ = layui.tool,
        _element_ = layui.element;
    var Navbar = function(options) {
        this.__render(options);
    };
    Navbar.fn = Navbar.prototype;
    Navbar.fn.getFilter = function() {
        return this._filter;
    };
    Navbar.fn.__render = function(options) {
        if(this._initialized) {
            layui.hint().error('Navbar已经初始化！');
            return;
        }
        options = options || {};

        //判断是否配置导航容器元素
        if(!!options.elem) {
            this._container = $(options.elem);
            //存在配置的容器
            if(!this._container.length) {
                this._container = undefined;
            }
        }
        if(!this._container) {
            //选取默认容器
            this._container = _body_.find('ul[gene-navbar]');
        }

        if(!this._container || !this._container.length) {
            layui.hint().error('Navbar没有正常初始化！');
            return;
        }
        this._container = this._container.eq(0);
        this._filter = options.filter || _tool_.makeHashCode('geneNav');

        var _that = this,
            _data = options.data || [],
            _elem = this._container,
            _showTip = !!options.showTip,
            _bg = options.bg,
            _navTree = !!options.navTree,
            _navSide = !!options.navSide,
            //_tipIndex = -1,
            _mouseEnter = function() {
                //_tipIndex =
                layer.tips($(this).children('span').text(), this);
            },
            //_mouseLeave = function() {
            //    _tipIndex > -1 && layer.close(_tipIndex);
            //},
            _beforeClick = typeof options.beforeClick === 'function' ? options.beforeClick : $.noop,
            _click = typeof options.click === 'function' ? options.click : $.noop;

        _elem.empty();
        _elem.addClass('layui-nav');
        _elem.attr('lay-filter', _that._filter);
        //垂直导航
        if(_navTree || _navSide) {
            _elem.addClass('layui-nav-tree');
            //展开子菜单时，是否收缩兄弟节点已展开的子菜单
            if(!!options.shrink) {
                _elem.attr('lay-shrink', 'all');
            }
        }
        //侧边栏
        if(_navSide) {
            _elem.addClass('layui-nav-side');
        }
        //背景色
        if(_bg) {
            _elem.addClass('layui-bg-' + _bg);
        }

        function createAEle(item) {
            var _a = $('<a href="javascript:;"></a>');
            if(!!item.event) {
                _a.attr('gene-event', item.event);
            }
            if(!!item.href) {
                _a.attr('href', item.href);
            }
            if(!!item.attr) {
                _a.attr(item.attr);
            }
            if(!!item.icon) {
                _a.html('<i class="layui-icon">' + item.icon + '</i>');
            } else if(!!item.eIcon) {
                _a.html(item.eIcon);
            }
            $('<span></span>').html(item.title).appendTo(_a);
            if(_showTip) {
                _a.attr('gene-navbar-hover', '');
            }
            return _a;
        }

        function buildSubNav(item) {
            var _dd = $('<dd></dd>'),
                _hasChildren = !!item.children && !!item.children.length;
            var _a = createAEle(item);
            _dd.append(_a);
            //如果没有子元素，则允许选择
            if(!_hasChildren) {
                if(!!item.select) {
                    _dd.addClass('layui-this');
                }
            } else {
                //存在子元素，则允许展开
                if(!!item.itemed) {
                    _dd.addClass('layui-nav-itemed');
                }
                _a.attr('href', 'javascript:;');
                var _dl = $('<dl class="layui-nav-child"></dl>');
                $.each(item.children, function(childIndex, child) {
                    _dl.append(buildSubNav(child));
                });
                _dd.append(_dl);
            }
            return _dd;
        }

        $.each(_data, function(index, item) {
            var _li = $('<li class="layui-nav-item"></li>'),
                _hasChildren = !!item.children && !!item.children.length;

            var _a = createAEle(item);
            _li.append(_a);
            //如果没有子元素，则允许选择
            if(!_hasChildren) {
                if(!!item.unselect) {
                    _li.attr('lay-unselect', '');
                } else if(!!item.select) {
                    _li.addClass('layui-this');
                }
            } else {
                //存在子元素，则允许展开
                if(!!item.itemed) {
                    _li.addClass('layui-nav-itemed');
                }
                _a.attr('href', 'javascript:;');
                var _dl = $('<dl class="layui-nav-child"></dl>');
                $.each(item.children, function(childIndex, child) {
                    _dl.append(buildSubNav(child));
                });
                _li.append(_dl);
            }
            _elem.append(_li);
        });
        if(_showTip) {
            _elem.on('mouseenter', 'a[gene-navbar-hover]', _mouseEnter);
        }
        _element_.init('nav', _that._filter);
        _element_.on('nav(' + _that._filter + ')', function(elem) {
            var event = elem.attr('gene-event');
            if(_beforeClick(elem, event) !== false) {
                _click(elem, event);
            };
        });

        _that._initialized = true;
        return _that;
    };

    function NavbarExport() {}
    NavbarExport.prototype.render = function(options) {
        return new Navbar(options);
    };
    exports('navbar', new NavbarExport());
});