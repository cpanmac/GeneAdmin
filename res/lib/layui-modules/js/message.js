/**
 * Name:message.js
 * Author:gene.jiang
 * E-mail:1013195469@qq.com
 * Website:http://www.jianggujin.com/
 * LICENSE:Apache v2 License
 * Description:Message提示
 */
layui.define(['jquery', 'geneconfig'], function(exports) {
    var $ = layui.jquery,
        _geneconfig_ = layui.geneconfig,
        _body_ = $('body'),
        _message_ = $('.gene-message');
    layui.link(_geneconfig_.getModulePath() + '/css/message.css');
    if(!!!_message_.length) {
        _message_ = $('<div class="gene-message"></div>');
        _body_.append(_message_);
    }
    var message = {
        show: function(options) {
            if(typeof options == 'string') {
                options = {
                    msg: options
                };
            }
            options = options || {};
            var _that = this,
                _timeoutFn = -1,
                _skin = options.skin || 'default',
                _color = options.color,
                _msg = options.msg || '提示信息!',
                _autoClose = options.autoClose === false ? false : true,
                _timeout = options.timeout || 3000,
                _messageItem = $('<div class="gene-message-item layui-anim layui-anim-upbit"></div>'),
                _messageBody = $('<div class="gene-message-body"></div>'),
                _messageClose = $('<div class="gene-close"><i class="layui-icon">&#x1006;</i></div>');
            if(_color) {
                _messageBody.css('color', _color);
                _messageClose.css('color', _color);
            } else {
                _messageBody.addClass('gene-skin-' + _skin);
                _messageClose.addClass('gene-skin-' + _skin);
            }
            _messageBody.html(_msg);
            _messageItem.append(_messageBody);
            _messageItem.append(_messageClose);
            _message_.append(_messageItem);

            function clearTimeoutFn() {
                if(_timeoutFn != -1) {
                    clearTimeout(_timeoutFn);
                    _timeoutFn = -1;
                }
            }

            function hideMsg() {
                _messageItem.removeClass('layui-anim-upbit').addClass('layui-anim-fadeout');
                _messageItem.hide("normal", function() {
                    _messageItem.remove();
                });
            }

            _messageClose.off('click').on('click', function() {
                clearTimeoutFn();
                hideMsg();
            });
            if(_autoClose) { //是否自动关闭
                _timeoutFn = setTimeout(hideMsg, _timeout);
                _messageItem.hover(clearTimeoutFn, function() {
                    _timeoutFn = setTimeout(hideMsg, _timeout);
                });
            }
        }
    };
    exports('message', message);
});