/**
 * Name:admin.js
 * Author:gene.jiang
 * E-mail:1013195469@qq.com
 * Website:http://www.jianggujin.com/
 * LICENSE:Apache v2 License
 * Description:管理端
 */
layui.define(['jquery', 'navbar', 'topnav', 'nprogress', 'geneconfig'], function(exports) {
    var $ = layui.jquery,
        _navbar_ = layui.navbar,
        _topnav_ = layui.topnav,
        _geneconfig_ = layui.geneconfig,
        _body_ = $('body'),
        _responseCssLoaded_ = false;
    //先引入admin.css
    layui.link(_geneconfig_.getModulePath() + '/css/admin.css');
    var Admin = function(options) {
        this.__render(options);
    };
    Admin.fn = Admin.prototype;
    Admin.fn.__render = function(options) {
        if(this._initialized) {
            layui.hint().error('Admin已经初始化！');
            return;
        }
        options = options || {};
        if(!!options.elem) {
            this._container = $(options.elem);
            //存在配置的容器
            if(!this._container.length) {
                this._container = undefined;
            }
        }
        if(!this._container) {
            //选取默认容器
            this._container = _body_.find('div[gene-admin]');
        }
        if(!this._container || !this._container.length) {
            layui.hint().error('Admin没有正常初始化！');
            return;
        }
        this._container = this._container.eq(0);
        this._collapse = typeof options.collapse === 'function' ? options.collapse : $.noop;
        this._expand = typeof options.expand === 'function' ? options.expand : $.noop;
        this._collapsed = false;

        var _that = this,
            _elem = this._container,
            _hasFooter = !!options.footer,
            _header = $('<div class="layui-header" gene-top-nav></div>'),
            _side = _that._side = $('<div class="layui-side layui-bg-black gene-side"></div>'),
            _navbar = $('<div class="layui-side-scroll"></div>'),
            _body = _that._body = $('<div class="layui-body"></div>'),
            _footer = null;

        _elem.empty();
        _elem.addClass('layui-layout layui-layout-admin gene-layout-admin');
        _elem.append(_header);
        _side.append(_navbar);
        _elem.append(_side);
        _elem.append(_body);

        //底部特殊处理，不一定有
        if(_hasFooter) {
            _footer = _that._footer = $('<div class="layui-footer"></div>');
            _footer.append(options.footer);
            _elem.append(_footer);
        } else {
            //设置为0px，底部会有空白
            _body.css('bottom', '-3px');
        }
        //侧边栏
        if(options.navbar) {
            var _hasFold = options.hasFold === false ? false : true;
            if(_hasFold) {
                if(!_responseCssLoaded_) {
                    layui.link(_geneconfig_.getModulePath() + '/css/admin.response.css');
                    _responseCssLoaded_ = true;
                }
                //展开/收缩
                var _fold = $('<div class="gene-side-fold"></div>');
                if(options.foldIcon) {
                    _fold.html('<i class="layui-icon">' + options.foldIcon + '</i>');
                } else if(options.eFoldIcon) {
                    _fold.html(options.eFoldIcon);
                } else {
                    //default
                    _fold.html('<i class="layui-icon">&#xe65f;</i>')
                }
                _navbar.append(_fold);
                //渲染导航菜单展开/收缩控制

                _fold.off('click').on('click', function() {
                    _that.toogle();
                });
            }
            var _ul = $('<ul gene-navbar></ul>');
            _navbar.append(_ul);

            _that._navbar = _navbar_.render($.extend(true, {
                navTree: true,
                showTip: true
            }, options.navbar, {
                elem: _ul
            }));
        }
        //顶部导航
        if(options.topnav) {
            _that._topnav = _topnav_.render($.extend(true, {}, options.topnav, {
                elem: _header
            }));
        }
        //中部内容
        if(options.content) {
            var _div = $('<div class="gene-body"></div>');
            _div.append($(options.content).html());
            _body.append(_div);
        } else if(options.src) {
            _that.setSrc(options.src);
        }

        _that._initialized = true;

        //收缩
        if(!!options.collapsed) {
            _that.collapse();
        }
        return _that;
    };
    Admin.fn.__initFrame = function() {
        var _that = this;
        if(!_that._frame) {
            var _body = _that._body;
            _body.empty();
            var _div = $('<div class="gene-body"></div>');
            _that._frame = $('<iframe frameborder="no" border="0" class="gene-frame"></iframe>');
            _div.append(_that._frame);
            _body.append(_div);
            _that._frame.on('load', function() {
                NProgress.done();
            });
        }
    }

    /**
     * 是否为收缩状态
     */
    Admin.fn.isCollapsed = function() {
        if(!this._initialized) {
            return;
        }
        return this._collapsed;
    };
    Admin.fn.toogle = function() {
        if(!this._initialized) {
            return;
        }
        if(this._collapsed) {
            this.expand();
        } else {
            this.collapse();
        }
    };
    /**
     * 收缩
     */
    Admin.fn.collapse = function() {
        if(!this._initialized) {
            return;
        }
        this._collapsed = true;
        this._side && this._side.addClass('gene-sided');
        this._body && this._body.addClass('gene-body-folded');
        this._footer && this._footer.addClass('gene-footer-folded');
        this._collapse();
    };
    /**
     * 展开
     */
    Admin.fn.expand = function() {
        if(!this._initialized) {
            return;
        }
        this._collapsed = false;
        this._side && this._side.removeClass('gene-sided');
        this._body && this._body.removeClass('gene-body-folded');
        this._footer && this._footer.removeClass('gene-footer-folded');
        this._expand();
    };

    Admin.fn.getNavbar = function() {
        return this._navbar;
    };
    Admin.fn.getTopnav = function() {
        return this._topnav;
    };
    Admin.fn.setSrc = function(url) {
        NProgress.start();
        this.__initFrame();
        this._frame.attr('src', url);
    };
    Admin.fn.setSrc2 = function(url) {
        var _that = this;
        _that.setSrc("");
        setTimeout(function() {
            _that.setSrc(url);
        })
    };
    Admin.fn.getSrc = function() {
        return this._frame && this._frame.attr('src');
    };
    Admin.fn.getFooter = function() {
        return this._footer;
    };

    function AdminExport() {}
    AdminExport.prototype.render = function(options) {
        return new Admin(options);
    };
    exports('admin', new AdminExport());
});