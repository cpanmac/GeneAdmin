/**
 * Name:geneconfig.js
 * Author:gene.jiang
 * E-mail:1013195469@qq.com
 * Website:http://www.jianggujin.com/
 * LICENSE:Apache v2 License
 * Description:配置文件
 */
layui.define(function(exports) {

    var geneconfig = {
        context: '/GeneAdmin', //系统上下文路径
        modules: '/res/lib/layui-modules', //自定义模块路径
        getModulePath: function() {
            return this.context + this.modules;
        },
        v: 'V1.0.0'
    };
    if(typeof _context != 'undefined') {
        geneconfig.context = _context;
    }
    if(typeof _mudules != 'undefined') {
        geneconfig.modules = _modules;
    }

    exports('geneconfig', geneconfig);
});