/**
 * Name:request.js
 * Author:gene.jiang
 * E-mail:1013195469@qq.com
 * Website:http://www.jianggujin.com/
 * LICENSE:Apache v2 License
 * Description:提供常用的Location对象的封装，同时为了区分，所以取名为request
 */
layui.define(['multivalue'], function(exports) {
    var _location_ = window.location,
        _multivalue_ = layui.multivalue;

    function parse(str) {
        var _arr = str.split('&'),
            _multivalue = _multivalue_.newMultiValue(),
            _tmp;
        for(var i = 0; i < _arr.length; i++) {
            _tmp = _arr[i].split('=');
            _multivalue.put(_tmp[0], decodeURIComponent(_tmp[1]));
        };
        return _multivalue;
    }

    var Request = function() {
        for(var property in _location_) {
            this[property] = _location_[property];
        }
        if(!!this.search) {
            this.queryString = this.search.substr(1);
            this.queryParams = parse(this.queryString);
        }

        this.rehash();
    };

    Request.fn = Request.prototype;
    /**
     * 重新解析hash值
     */
    Request.fn.rehash = function() {
        if(!!this.hash) {
            this.hashString = this.hash.substr(1);
            this.hashParams = parse(this.hash);
        }
    };
    /**
     * 获得请求参数
     * @param {Object} name
     */
    Request.fn.getPara = function(name, defVal) {
        return (this.queryParams && this.queryParams.getFirst(name)) || defVal;
    };
    Request.fn.getParas = function(name) {
        return this.queryParams && this.queryParams.get(name);
    };
    /**
     * 获得hash参数
     * @param {Object} name
     */
    Request.fn.getHashPara = function(name, defVal) {
        return (this.hashParams && this.hashParams.getFirst(name)) || defVal;
    };
    Request.fn.getHashParas = function(name) {
        return this.hashParams && this.hashParams.get(name);
    };

    exports('request', new Request());
});