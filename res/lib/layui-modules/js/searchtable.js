/**
 * Name:searchtable.js
 * Author:gene.jiang
 * E-mail:1013195469@qq.com
 * Website:http://www.jianggujin.com/
 * LICENSE:Apache v2 License
 * Description:搜索页面模块
 */
layui.define(['jquery', 'search', 'table', 'tool'], function(exports) {
    var $ = layui.jquery,
        _search_ = layui.search,
        _tool_ = layui.tool,
        _table_ = layui.table,
        _geneconfig_ = layui.geneconfig,
        _body_ = $('body');
    var SearchTable = function(options) {
        this.__render(options);
    };
    SearchTable.fn = SearchTable.prototype;

    SearchTable.fn.__render = function(options) {
        if(this._initialized) {
            layui.hint().error('SearchTable已经初始化！');
            return;
        }
        options = options || {};
        if(!!options.elem) {
            this._container = $(options.elem);
            //存在配置的容器
            if(!this._container.length) {
                this._container = undefined;
            }
        }
        if(!this._container) {
            //选取默认容器
            this._container = _body_.find('div[gene-search-table]');
        }
        if(!this._container || !this._container.length) {
            layui.hint().error('SearchTable没有正常初始化！');
            return;
        }
        this._container = this._container.eq(0);
        var _that = this,
            _elem = this._container,
            _callback = options.callback || {};

        _elem.empty();

        //搜索模块
        var _searchOpt = options.search;
        if(!!_searchOpt) {
            var _search = $('<div></div>'),
                _realSearchOpt = {};
            _elem.append(_search);
            _tool_.shadowCopy(_realSearchOpt, _searchOpt);
            _realSearchOpt.elem = _search;
            _realSearchOpt.callback = {
                onClick: _callback.onClick,
                onSearch: function(multivalue) {
                    var args = [].slice.call(arguments, 0),
                        _custom = _callback.onSearch && _callback.onSearch.apply(options, args);
                    //用户没有拦截，执行默认逻辑
                    if(_custom !== false) {
                        _that._table && _that._table.reload({
                            where: multivalue.toSingleValueMap()
                        });
                    }
                }
            };
            _that._search = _search_.render(_realSearchOpt);
        }
        //表格
        var _tableOpt = options.table;
        if(!!_tableOpt) {
            var _table = $('<table class="layui-table"></table>'),
                _filter = _that._filter = _tool_.makeHashCode('searchPageTable'),
                _realTableOpt = {};
            _elem.append($('<div class="gene-table"></div>').append(_table));
            _tool_.shadowCopy(_realTableOpt, _tableOpt);
            _realTableOpt.elem = 'table[lay-filter="' + _filter + '"]';
            _table.attr('lay-filter', _filter);
            if(typeof _callback.onDone == 'function') {
                _realTableOpt.done = function(res, curr, count) {
                    if(typeof _tableOpt.done == 'function') {
                        _tableOpt.done(res, curr, count);
                    }
                    _callback.onDone(res, curr, count);
                }
            }
            _that._table = _table_.render(_realTableOpt);
            _callback.onCheckbox && _table_.on('checkbox(' + _filter + ')', _callback.onCheckbox);
            _callback.onEdit && _table_.on('edit(' + _filter + ')', _callback.onEdit);
            _callback.onSort && _table_.on('sort(' + _filter + ')', _callback.onSort);
            _callback.onRow && _table_.on('row(' + _filter + ')', _callback.onRow);
            if(_callback.onTool || options.tool) {
                var _toolEvent = options.tool;
                _table_.on('tool(' + _filter + ')', function(obj) {
                    var _layEvent = obj.event;
                    _callback.onTool && _callback.onTool(obj);
                    if(_toolEvent && (typeof _toolEvent[_layEvent] == 'function')) {
                        _toolEvent[_layEvent](obj);
                    }
                });
            }
        }

        _that._initialized = true;
        return _that;
    };
    SearchTable.fn.getSearch = function() {
        return this._search;
    };
    SearchTable.fn.getTable = function() {
        return this._table;
    }
    SearchTable.fn.getTableFilter = function() {
        return this._filter;
    }

    function SearchTableExport() {}
    SearchTableExport.prototype.render = function(options) {
        return new SearchTable(options);
    };
    exports('searchtable', new SearchTableExport());
});