/**
 * Name:storage.js
 * Author:gene.jiang
 * E-mail:1013195469@qq.com
 * Website:http://www.jianggujin.com/
 * LICENSE:Apache v2 License
 */
layui.define(['tool'], function(exports) {
    function CookieStorage(session) {
        this.options = {
            path: '/'
        };
        if(session === false) {
            this.options.expires = new Date(9999, 11, 31, 23, 59, 59);
        }
    }
    CookieStorage.prototype = {
        constructor: CookieStorage,
        getItem: function(name) {
            return Common.prototype.cookie(name);
        },
        setItem: function(name, value) {
            return Common.prototype.cookie(name, value, {
                expires: new Date(9999, 11, 31, 23, 59, 59),
                path: '/'
            });
        },
        removeItem: function(name) {
            return Common.prototype.removeCookie(name);
        }
    };
    /**
     * 数据存储
     */
    function DataStorage(session) {
        this.storage = (session === false ? window.localStorage : window.sessionStorage) || new CookieStorage(session);
    }
    DataStorage.prototype = {
        constructor: DataStorage,
        getItem: function(name) {
            return this.storage.getItem(name);
        },
        setItem: function(name, value) {
            return this.storage.setItem(name, value);
        },
        removeItem: function(name) {
            return this.storage.removeItem(name);
        }
    };

    function StorageExport() {}
    StorageExport.prototype.newStorage = function(session) {
        return new DataStorage(session);
    }
    exports('storage', new StorageExport());
});