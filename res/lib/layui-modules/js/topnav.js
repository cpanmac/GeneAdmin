/**
 * Name:topnav.js
 * Author:gene.jiang
 * E-mail:1013195469@qq.com
 * Website:http://www.jianggujin.com/
 * LICENSE:Apache v2 License
 * Description:顶部导航
 */
layui.define(['jquery', 'element', 'tool'], function(exports) {
    var $ = layui.jquery,
        _body_ = $('body'),
        _tool_ = layui.tool,
        _element_ = layui.element;
    var Topnav = function(options) {
        this.__render(options);
    };
    Topnav.fn = Topnav.prototype;
    Topnav.fn.getLeftFilter = function() {
        return this._leftFilter;
    };
    Topnav.fn.getRightFilter = function() {
        return this._rightFilter;
    };
    Topnav.fn.__render = function(options) {
        if(this._initialized) {
            layui.hint().error('Topnav已经初始化！');
            return;
        }
        options = options || {};

        //判断是否配置导航容器元素
        if(!!options.elem) {
            this._container = $(options.elem);
            //存在配置的容器
            if(!this._container.length) {
                this._container = undefined;
            }
        }
        if(!this._container) {
            //选取默认容器
            this._container = _body_.find('div[gene-top-nav]');
        }
        if(!this._container || !this._container.length) {
            layui.hint().error('Topnav没有正常初始化！');
            return;
        }
        this._container = this._container.eq(0);

        this._leftFilter = options.leftFilter || _tool_.makeHashCode('geneTopNav');
        this._rightFilter = options.rightFilter || _tool_.makeHashCode('geneTopNav');

        var _that = this,
            _elem = this._container,
            _beforeClick = typeof options.beforeClick === 'function' ? options.beforeClick : $.noop,
            _click = typeof options.click === 'function' ? options.click : $.noop;
        _elem.empty();
        if(!!options.logo) {
            $('<div class="layui-logo"></div>').html(options.logo).appendTo(_elem);
        }
        if(!!options.mLogo) {
            $('<div class="layui-logo gene-logo-mobile"></div>').html(options.mLogo).appendTo(_elem);
        }

        function createNavItem(item) {
            var _a = $('<a href="javascript:;"></a>');
            if(!!item.event) {
                _a.attr('gene-event', item.event);
            }
            if(!!item.href) {
                _a.attr('href', item.href);
            }
            if(!!item.attr) {
                _a.attr(item.attr);
            }
            if(!!item.icon) {
                _a.html('<i class="layui-icon">' + item.icon + '</i>');
            } else if(!!item.eIcon) {
                _a.html(item.eIcon);
            }
            $('<span></span>').html(item.title).appendTo(_a);
            return _a;
        }

        //      function navItemBind(navItem) {
        //          navItem.off('click').on('click', function() {
        //              var event = navItem.attr('gene-event');
        //              if(_beforeClick(navItem, event) !== false) {
        //                  _click(navItem, event);
        //              };
        //          });
        //      }
        var navItems = [];
        _that._navItemPostions = [];
        if(!!options.left && options.left.length) {
            navItems.push(options.left);
            _that._navItemPostions.push('left');
        }
        if(!!options.right && options.right.length) {
            navItems.push(options.right);
            _that._navItemPostions.push('right');
        }
        $.each(navItems, function(_index, navItem) {
            var _ul = $('<ul class="layui-nav gene-top-nav">'),
                _filter = _that['_' + _that._navItemPostions[_index] + 'Filter'];
            _ul.attr('lay-filter', _filter);
            _ul.addClass('layui-layout-' + _that._navItemPostions[_index]);
            $.each(navItem, function(index, item) {
                var _li = $('<li class="layui-nav-item"></li>'),
                    _hasChildren = !!item.children && !!item.children.length;
                var _a = createNavItem(item);
                _li.append(_a);
                if(!_hasChildren) {
                    if(!!item.unselect) {
                        _li.attr('lay-unselect', '');
                    } else if(!!item.select) {
                        _li.addClass('layui-this');
                    }
                    // navItemBind(_a);
                } else {
                    _a.attr('href', 'javascript:;');
                    var _dl = $('<dl class="layui-nav-child"></dl>');
                    $.each(item.children, function(childIndex, child) {
                        var _dd = $('<dd></dd>');
                        if(!!child.select) {
                            _dd.addClass('layui-this');
                        }
                        var _dd_aa = createNavItem(child);
                        //navItemBind(_dd_aa);
                        _dd.append(_dd_aa);
                        _dl.append(_dd);
                    });
                    _li.append(_dl);
                }
                _ul.append(_li);
            });
            _elem.append(_ul);
            _element_.init('nav', _filter);
            _element_.on('nav(' + _filter + ')', function(elem) {
                var event = elem.attr('gene-event');
                if(_beforeClick(elem, event) !== false) {
                    _click(elem, event);
                };
            });
        });

        _that._initialized = true;
        return _that;
    };

    function TopnavExport() {}
    TopnavExport.prototype.render = function(options) {
        return new Topnav(options);
    }
    exports('topnav', new TopnavExport());
});