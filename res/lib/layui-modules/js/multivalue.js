/**
 * Name:multivalue.js
 * Author:gene.jiang
 * E-mail:1013195469@qq.com
 * Website:http://www.jianggujin.com/
 * LICENSE:Apache v2 License
 */
layui.define([], function(exports) {
    /**
     * 数组包裹，避免第一位添加的值为数组的时候，无法判断元素个数
     */
    function ArrayWrapper(arr) {
        this.targetArr = arr;
    }
    /**
     * 多值集合
     */
    function MultiValue() {
        this._keyMap = {};
    }
    MultiValue.fn = MultiValue.prototype;
    MultiValue.fn.constructor = MultiValue;
    /**
     * 添加数据
     * @param {Object} key
     * @param {Object} value
     */
    MultiValue.fn.put = function(key, value) {
        if(!!key) {
            if(this._keyMap.hasOwnProperty(key)) {
                var _oldVal = this._keyMap[key];
                if(_oldVal instanceof Array) {
                    _oldVal.push(value);
                } else {
                    this._keyMap[key] = [_oldVal, value];
                }
            } else {
                this._keyMap[key] = (value instanceof Array) ? new ArrayWrapper(value) : value;
            }
        }
    };
    /**
     * 移除数据
     * @param {Object} key
     */
    MultiValue.fn.remove = function(key) {
        if(!!key) {
            delete this._keyMap[key];
        }
    };

    function getTargetArr(obj) {
        return(obj instanceof ArrayWrapper) ? obj.targetArr : obj;
    }
    /**
     * 获得数据
     * @param {Object} key
     */
    MultiValue.fn.get = function(key) {
        if(!!key) {
            var _oldVal = this._keyMap[key];
            if(_oldVal instanceof Array) {
                var arr = [],
                    len = _oldVal.length;
                for(var i = 0; i < len; i++) {
                    arr.push(_oldVal[i]);
                }
                if(len > 0) {
                    arr[0] = getTargetArr(arr[0]);
                }
            }
            return getTargetArr(_oldVal);
        }
    };
    /**
     * 获得首个数据
     * @param {Object} key
     */
    MultiValue.fn.getFirst = function(key) {
        if(!!key) {
            var _oldVal = this._keyMap[key];
            if(_oldVal instanceof Array) {
                var len = _oldVal.length;
                if(len > 0) {
                    return getTargetArr(_oldVal[0]);
                }
                return;
            }
            return getTargetArr(_oldVal);
        }
    };
    MultiValue.fn.toMap = function() {
        var _signgleMap = {},
            _taht = this;
        for(var key in _taht._keyMap) {
            _signgleMap[key] = _taht.get(key);
        }
        return _signgleMap
    };
    MultiValue.fn.toSingleValueMap = function() {
        var _signgleMap = {},
            _taht = this;
        for(var key in _taht._keyMap) {
            _signgleMap[key] = _taht.getFirst(key);
        }
        return _signgleMap
    };

    function MultiValueExport() {}
    MultiValueExport.prototype.newMultiValue = function() {
        return new MultiValue();
    }
    exports('multivalue', new MultiValueExport());
});