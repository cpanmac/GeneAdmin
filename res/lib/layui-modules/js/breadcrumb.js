/**
 * Name:breadcrumb.js
 * Author:gene.jiang
 * E-mail:1013195469@qq.com
 * Website:http://www.jianggujin.com/
 * LICENSE:Apache v2 License
 */
layui.define(['jquery', 'element', 'tool'], function(exports) {
    var $ = jQuery = layui.jquery,
        _tool_ = layui.tool,
        _body_ = $('body'),
        _element_ = layui.element;
    var Breadcrumb = function(options) {
        this.__render(options);
    };
    Breadcrumb.fn = Breadcrumb.prototype;
    Breadcrumb.fn.getFilter = function() {
        return this._filter;
    };
    Breadcrumb.fn.__render = function(options) {
        if(this._initialized) {
            layui.hint().error('Breadcrumb已经初始化！');
            return;
        }
        options = options || {};

        //判断是否配置导航容器元素
        if(!!options.elem) {
            this._container = $(options.elem);
            //存在配置的容器
            if(!this._container.length) {
                this._container = undefined;
            }
        }
        if(!this._container) {
            //选取默认容器
            this._container = _body_.find('span[gene-breadcrumb]');
        }

        if(!this._container || !this._container.length) {
            layui.hint().error('Breadcrumb没有正常初始化！');
            return;
        }
        this._container = this._container.eq(0);

        this._filter = options.filter || _tool_.makeHashCode('geneBreadcrumb');

        var _that = this,
            _data = options.data || [],
            _elem = this._container;
        _elem.empty();
        _elem.addClass('layui-breadcrumb'); //separator
        if(options.separator) {
            _elem.attr('lay-separator', options.separator);
        }
        _elem.attr('lay-filter', _that._filter);
        $.each(_data, function(index, item) {
            var _a = $('<a></a>');
            if(typeof item == 'string') {
                _a.text(item).attr('href', 'javascript:;');
            } else {
                if(!!item.cite) {
                    $('<cite></cite>').text(item.title || '').appendTo(_a);
                } else {
                    _a.text(item.title || '');
                }
                _a.attr('href', item.href || 'javascript:;');
            }
            _elem.append(_a);
        });
        _element_.init('breadcrumb', _that._filter);
        _that._initialized = true;
        return _that;
    };

    function BreadcrumbExport() {}
    BreadcrumbExport.prototype.render = function(options) {
        return new Breadcrumb(options);
    };
    exports('breadcrumb', new BreadcrumbExport());
});