# 更新日志

## 1.0.4

- 升级layui版本为：`layui-v2.4.3`
- 新增标签输入框组件
- 修复树形选择部分情况下`separator`参数无效

## 1.0.3

- 删除日期时间范围输入框组件的`separator `与`sepWidth `参数
- 调整树形选择模块参数，详情见文档

## 1.0.2

- 升级layui版本为：`layui-v2.3.0`
- 表单(gform)模块`date`组件新增`isInitValue`参数
- 表单(gform)模块`dateRange`组件允许为每一个日期组件设置参数
- 选项卡(tab)模块不建议使用，并且不保证后续版本一直可使用
- 删除管理端(admin)模块的选项卡部分
- 管理端(admin)模块新增`content`、`src`参数和`setSrc()`、`setSrc2()`、`getSrc()`方法
- 新增ztree(ztree)模块
- 新增树形选择(treeselect)模块
- 新增多值数据类型(multivalue)模块
- 新增请求处理(request)模块
- 新增二维码(qrcode)模块
- 新增搜索表单(search)模块
- 新增搜索表单表格(searchtable)模块

## 1.0.1

- 管理端(admin)模块新增`collapsed`参数
- 新增表单(gform)模块

## 1.0.0

- 新增环境配置(geneconfig)模块
- 新增异步请求(ajax)模块
- 新增面包屑(breadcrumb)模块
- 新增消息(message)模块
- 新增导航(navbar)模块
- 新增顶部导航(topnav)模块
- 新增选项卡(tab)模块
- 新增管理端(admin)模块
- 新增数据缓存(cache)模块
- 新增JS Hack(hack)模块
- 新增加载进度(nprogress)模块
- 新增进度条(progress)模块
- 新增数据存储(storage)模块
- 新增常用工具(tool)模块
- 新增placeholder(placeholder)模块

## 孵化