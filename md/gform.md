# 表单

## 第一部分 模块说明

模块名称：`gform`

扩展layui的表单，可以通过配置生成表单。

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="../../res/lib/layui/css/layui.css" rel="stylesheet" />
    </head>

    <body>
        <div gene-form></div>
        <script type="text/javascript" src='../../res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: '../../res/lib/layui-modules/js/'
            }).use(['gform'], function() {
                var gform = layui.gform.render({
                    rows: [
                        [{
                            label: {
                                size: 2,
                                text: '文本一'
                            },
                            verify: 'required',
                            size: 4
                        }, {
                            label: {
                                size: 2,
                                text: '文本二'
                            },
                            verify: 'required',
                            size: 4
                        }],
                        {
                            label: '密码框',
                            type: 'password'
                        }
                    ]
                });
            });
        </script>
    </body>
</html>
```

## 第二部分 参数详情

我们可以通过`layui.gform.render()`方法渲染一个表单，并获得表单对象。render方法的参数如下：

### 2.1 elem

表单容器，默认选择`*[gene-form]`。

### 2.2 filter

元素的`lay-filter`的值，默认会自动生成一个随机值。

### 2.3 pane

表单方框风格，默认：`false`。

### 2.4 rows

表单的每一行数据。数据结构为数组，公共参数数据项参数如下：

- label {String|JSON} 输入项标签，可用参数如下：
  - text {String} 标签文本

  - size {Arrary|Number} 标签尺寸，即一行中的宽度，如果参数为数组，则可以指定在不同分辨率下所占宽度，可以是`xs`、 `sm`、 `md`和 `lg`与`1-12`的组合，示例：`['xs2', 'md2']`。如果在不同分辨率下宽度比例相同，我们可以直接指定`1-12`的宽度。默认上面两种都无效的时候，会为标签添加`layui-form-label`样式 

    > 如果label直接使用字符串作为参数，则相当于传递：`{text: text}`，如果label参数为空，则不会渲染标签，这通常在按钮组件的时候是必要的

- type {String} 组件类型，默认：`text`，所有可用组件与特殊参数如下：

  - text - 文本输入框组件
    - size {String|Number|Array} 组件尺寸，当参数类型为字符串时，可以使用：`block`与`inline`，默认使用`block`，i如果需要特殊控制不同分辨率下所占宽度，可以是`xs`、 `sm`、 `md`和 `lg`与`1-12`的组合，示例：`['xs2', 'md2']`。如果在不同分辨率下宽度比例相同，我们可以直接指定`1-12`的宽度
    - verify {String} 验证规则
    - ignore {Boolean} 忽略表单美化，默认：`false`
    - verType {String} 验证提示类型，可用值为：`tips（吸附层）`、`alert（对话框）`、`msg（默认）`
    - name {String} 组件元素name属性
    - placeholder {String} 组件元素placeholder 属性
    - attr {JSON} 自定义组件元素属性
  - textarea - 文本域组件，继承自`text`
  - password(pwd) - 密码输入框组件，继承自`text`
  - date - 日期时间输入框组件，继承自`text`
    - data {JSON} 日期和时间组件配置参数，参数详情如下：
      - type {String} 控件选择类型，用于单独提供不同的选择器类型，默认值：`date`，可用值如下：
        - `year`   年选择器，只提供年列表选择
        - `month`   年月选择器，只提供年、月选择
        - `date`    日期选择器，可选择：年、月、日。type默认值，一般可不填
        - `time`   时间选择器，只提供时、分、秒选择
        - `datetime`  日期时间选择器，可选择：年、月、日、时、分、秒
      - range {Boolean|String} 开启左右面板范围选择，默认值：`false`，如果设置 `true`，将默认采用 ` -` 分割。 你也可以直接设置 `分割字符`。五种选择器类型均支持左右面板的范围选择。
      - format {String} 自定义格式，默认值：`yyyy-MM-dd`，通过日期时间各自的格式符和长度，来设定一个你所需要的日期格式。支持的格式如下：
        - `yyyy` 年份，至少四位数。如果不足四位，则前面补零
        - `y` 年份，不限制位数，即不管年份多少位，前面均不补零
        - `MM` 月份，至少两位数。如果不足两位，则前面补零
        - `M` 月份，允许一位数
        - `dd` 日期，至少两位数。如果不足两位，则前面补零
        - `d` 日期，允许一位数
        - `HH` 小时，至少两位数。如果不足两位，则前面补零
        - `H` 小时，允许一位数
        - `mm` 分钟，至少两位数。如果不足两位，则前面补零
        - `m` 分钟，允许一位数
        - `ss` 秒数，至少两位数。如果不足两位，则前面补零
        - `s` 秒数，允许一位数
      - value {String} 初始值，默认值：`new Date()`，支持传入符合`format`参数设定的日期格式字符，或者` new Date()`
      - isInitValue {Boolean} 初始值填充，默认值：`false`，用于控制是否自动向元素填充初始值（需配合 `value` 参数使用）
      - min/max {string} 最小/大范围内的日期时间值，默认值：`min: '1900-1-1'`、`max: '2099-12-31'`，设定有限范围内的日期或时间值，*不在范围内的将不可选中*。这两个参数的赋值非常灵活，主要有以下几种情况：

        - 如果值为字符类型，则：*年月日必须用 -（中划线）分割*、*时分秒必须用 :（半角冒号）号分割*。这里并非遵循 format 设定的格式
        - 如果值为整数类型，且数字＜86400000，则数字代表天数，如：min: -7，即代表最小日期在7天前，正数代表若干天后
        - 如果值为整数类型，且数字 ≥ 86400000，则数字代表时间戳，如：max: 4073558400000，即代表最大日期在：公元3000年1月1日
      - trigger {String} 自定义弹出控件的事件，默认值：`focus`
      - show {Boolean} 默认显示，默认值：`false`，如果设置: `true`，则控件默认显示在绑定元素的区域
      - position {String} 定位方式，默认值：`absolute`，用于设定控件的定位方式，有以下三种可选值：
        - abolute 绝对定位，始终吸附在绑定元素周围。默认值 
        - fixed  固定定位，初始吸附在绑定元素周围，不随浏览器滚动条所左右。一般用于在固定定位的弹层中使用
        - static 静态定位，控件将直接嵌套在指定容器中。 注意：*请勿与 show 参数的概念搞混淆。show为 true 时，控件仍然是采用绝对或固定定位。而这里是直接嵌套显示* 
      - zIndex {Number} 层叠顺序，默认值：`66666666`，一般用于解决与其它元素的互相被遮掩的问题。如果 position 参数设为 static 时，该参数无效。
      - showBottom {Boolean} 是否显示底部栏，默认值：`true`，如果设置 `false`，将不会显示控件的底部栏区域
      - btns {Array} 工具按钮，默认值：`['clear', 'now', 'confirm']`，右下角显示的按钮，会按照数组顺序排列，内置可识别的值有：`clear`、`now`、`confirm`
      - lang {String} 语言，默认值：`cn`，我们内置了两种语言版本：`cn`（中文版）、`en`（国际版，即英文版）
      - theme {String} 主题，默认值：`default`，我们内置了多种主题，theme的可选值有：`default`（默认简约）、`molv`（墨绿背景）、`#颜色值`（自定义颜色背景）、`grid`（格子主题）
      - calendar {Boolean} 是否显示公历节，默认值：`false`，我们内置了一些我国通用的公历重要节日，通过设置 `true`来开启。国际版不会显示
      - mark {Object} 标注重要日子，calendar 参数所代表的公历节日更多情况下是一个摆设。因此，我们还需要自定义标注重要日子，比如结婚纪念日？日程等？

        - 每年的日期： {'0-9-18': '国耻'}， `0` 即代表每一年
        - 每月的日期： {'0-0-15': '中旬'}，`0-0` 即代表每年每月
        - 特定的日期： {'2017-8-21': '发布')  
      - ready { Function} 控件初始打开的回调，控件在打开时触发，回调返回一个参数：初始的日期时间对象
      - change { Function} 日期时间被切换后的回调，年月日时间被切换时都会触发。回调返回三个参数，分别代表：`生成的值`、`日期时间对象`、`结束的日期时间对象`
      - done { Function} 控件选择完毕后的回调，点击日期、清空、现在、确定均会触发。回调返回三个参数，分别代表：`生成的值`、`日期时间对象`、`结束的日期时间对象`
  - dateRange - 日期时间范围输入框组件，会渲染两个日期时间输入框组件
    - size {String|Number|Array} 组件尺寸，当参数类型为字符串时，可以使用：`block`与`inline`，默认使用`block`，i如果需要特殊控制不同分辨率下所占宽度，可以是`xs`、 `sm`、 `md`和 `lg`与`1-12`的组合，示例：`['xs2', 'md2']`。如果在不同分辨率下宽度比例相同，我们可以直接指定`1-12`的宽度
    - verify {String|Array} 验证规则，如果参数为数组，则可以分别设置
    - verType {String|Array} 验证提示类型，可用值为：`tips（吸附层）`、`alert（对话框）`、`msg（默认），如果参数为数组，则可以分别设置`
    - name {String|Array} 组件元素name属性，如果参数为数组，则可以分别设置
    - placeholder {String|Array} 组件元素placeholder 属性，如果参数为数组，则可以分别设置
    - attr {JSON|Array} 自定义组件元素属性，如果参数为数组，则可以分别设置
    - data  {JSON|Array} 日期和时间组件配置参数，如果参数为数组，则可以分别设置，参数值与日期时间输入框组件的`data`参数相同
  - select - 选择输入框组件
    - size {String|Number|Array} 组件尺寸，当参数类型为字符串时，可以使用：`block`与`inline`，默认使用`block`，i如果需要特殊控制不同分辨率下所占宽度，可以是`xs`、 `sm`、 `md`和 `lg`与`1-12`的组合，示例：`['xs2', 'md2']`。如果在不同分辨率下宽度比例相同，我们可以直接指定`1-12`的宽度
    - verify {String} 验证规则
    - ignore {Boolean} 忽略表单美化，默认：`false`
    - verType {String} 验证提示类型，可用值为：`tips（吸附层）`、`alert（对话框）`、`msg（默认）`
    - name {String} 组件元素name属性
    - placeholder {String} 设置该参数会默认添加一个空的选项，用于提示
    - attr {JSON} 自定义组件元素属性
    - text {String} 修改选项文本对应的键，默认为：`text`
    - value {String} 修改选项值对应的键，默认为：`value`
    - data {Array} 选项数据，例如：`[{text:'南京', value: 'nj'}]`

    > 从1.0.2开始，`data`中的每一项数据可以包含`disabled `和`selected`，用于控制是否禁用和默认值，如果选项需要开启分组，则`data`中的数据项设置为字符串即可，结束位置设置为`false`,例如：`['分组一', {text:'南京', value: 'nj'}, false, {text:'北京', value: 'bj'}]`。
  - radio - 单选框组件，支持多个
    - size {String|Number|Array} 组件尺寸，当参数类型为字符串时，可以使用：`block`与`inline`，默认使用`block`，i如果需要特殊控制不同分辨率下所占宽度，可以是`xs`、 `sm`、 `md`和 `lg`与`1-12`的组合，示例：`['xs2', 'md2']`。如果在不同分辨率下宽度比例相同，我们可以直接指定`1-12`的宽度
    - data {Array|JSON} 单选数据数组，每一个单选框对应数组中一个对象，如果参数为普通的JSON对象，则只会渲染一个，详细参数如下：
      - verify {String} 验证规则
      - ignore {Boolean} 忽略表单美化，默认：`false`
      - verType {String} 验证提示类型，可用值为：`tips（吸附层）`、`alert（对话框）`、`msg（默认）`
      - name {String} 组件元素name属性
      - value {String} 组件元素value 属性
      - title {String} 组件元素title 属性，建议设置该属性，否则渲染后无提示文本显示
      - checked {String} 组件是否选中
      - attr {JSON} 自定义组件元素属性
  - checkbox - 复选框组件，继承自`radio `，在radio的基础上，checkbox的data参数扩展了如下参数：
    - primary {Boolean} 原始风格，默认：`false`
    - switch {Boolean} 开关风格，默认：`false`
    - text {String} 自定义开关两种状态的文本 ，例如：`ON|OFF`
  - button- 按钮组件，支持多个
    - size {String|Number|Array} 组件尺寸，当参数类型为字符串时，可以使用：`block`与`inline`，默认使用`block`，i如果需要特殊控制不同分辨率下所占宽度，可以是`xs`、 `sm`、 `md`和 `lg`与`1-12`的组合，示例：`['xs2', 'md2']`。如果在不同分辨率下宽度比例相同，我们可以直接指定`1-12`的宽度
    - center {Boolean} 是否居中显示，默认：`false`
    - data {Array|JSON} 单选数据数组，每一个单选框对应数组中一个对象，如果参数为普通的JSON对象，则只会渲染一个，详细参数如下：
      - submit {Boolean}是否为提交按钮
      - theme {String} 按钮主题，可用值有：`primary(原始)`、`normal(百搭)`、`warm(暖色)`、`danger(警告)`、`disabled(禁用)`
      - name {String} 组件元素name属性
      - text {String} 组件元素text属性
      - attr {JSON} 自定义组件元素属性

## 第三部分 表单对象方法

### 3.1 getFilter()

获取表单元素的`lay-filter=""`值。

### 3.2 serialize(options)

序列化表单，转换成key=value形式字符串，`options`为JSON数据，参数如下：

- trimEmptyVal {Boolean} 移除值为空字符串的项
- supportArrayName {Boolean}支持数组name，即：name[]会变成name[0]

### 3.3 serializeArray(options)

序列化表单，转换成数组，数组每一项为：`{name:name,value:value}`，`options`为JSON数据，参数如下：

- trimEmptyVal {Boolean} 移除值为空字符串的项
- supportArrayName {Boolean}支持数组name，即：name[]会变成name[0]

### 3.3 serializeFieldValue(options)

序列化表单，转换为`multivalue`对象，`options`为JSON数据，参数如下：

- trimEmptyVal {Boolean} 移除值为空字符串的项
- supportArrayName {Boolean}支持数组name，即：name[]会变成name[0]

### 3.5 verify()

表单校验，返回Boolean数据，返回`true`则表示校验通过

### 3.6 val(name, [value])

表单值读写器，参数如下：

- name {String} 需要读取或设置的输入项`name`属性值

- value {String} 如果传递该参数则为设置name对应元素的值

  > 如果元素是需要动态更新的，不需要再调用`form.render`方法，模块会自动判断是否需要重新渲染

### 3.7 resetSelect(name, options, defVal)

重置选择组件的选项，可以对选择组件重新渲染，参数如下：

- name {String} 需要重新渲染的选择组件的`name`属性值
- options {JSON}重新渲染参数，参数详情如下：
  - placeholder {String} 设置该参数会默认添加一个空的选项，用于提示
  - attr {JSON} 自定义组件元素属性
  - text {String} 修改选项文本对应的键，默认为：`text`
  - value {String} 修改选项值对应的键，默认为：`value`
  - data {Array} 选项数据，例如：`[{text:'南京', value: 'nj'}]`
- defVal {String} 默认值

## 第四部分 自定义输入组件

我们可以通过`layui.gform.component()`方法获取或设置新的输入组件，大部分情况下，我们不需要自定义组件，如果默认组件无法满足需求，可以查看示例自定义输入组件，这里不做详细描述。

## 第五部分 示例

### 5.1 常规用法

```html
<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <title></title>
        <link href="../../res/lib/layui/css/layui.css" rel="stylesheet" />
    </head>

    <body>
        <div gene-form></div>
        <script type="text/javascript" src='../../res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: '../../res/lib/layui-modules/js/'
            }).use(['gform'], function() {
                window.$ = window.jQuery = layui.jquery;
                var gform = layui.gform.render({
                    rows: [
                        [{
                            label: {
                                size: 2,
                                text: '文本一'
                            },
                            verify: 'required',
                            size: 4
                        }, {
                            label: {
                                size: 2,
                                text: '文本二'
                            },
                            verify: 'required',
                            size: 4
                        }],
                        {
                            label: '密码框',
                            type: 'password'
                        },
                        {
                            label: '选择框',
                            type: 'select',
                            data: [{
                                text: '选项一',
                                value: '1'
                            }]
                        },
                        {
                            label: '文本域',
                            type: 'textarea'
                        },
                        {
                            label: '时间',
                            type: 'date',
                            data: {
                                theme: 'grid'
                            }
                        }, {
                            label: '时间区间',
                            type: 'dateRange'
                        }, {
                            label: '单选框',
                            type: 'radio',
                            data: [{
                                name: 'sex',
                                value: '1',
                                title: '男',
                                checked: true
                            }, {
                                name: 'sex',
                                title: '女',
                                value: '0'
                            }]
                        }, {
                            label: '复选框',
                            type: 'checkbox',
                            data: [{
                                name: 'c1',
                                value: '1',
                                title: '选框一',
                                checked: true
                            }, {
                                name: 'c2',
                                value: '2',
                                title: '选项二',
                                primary: true
                            }, {
                                name: 'c3',
                                value: '3',
                                title: '选项三',
                                'switch': true
                            }]
                        }, {
                            type: 'button',
                            center: true,
                            size: 12,
                            data: [{
                                text: '立即提交',
                                submit: true
                            }, {
                                theme: 'primary',
                                text: '重置'
                            }]
                        }
                    ]
                });
            });
        </script>
    </body>

</html>
```
### 5.2 自定义组件

```html
<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <title></title>
        <link href="../../res/lib/layui/css/layui.css" rel="stylesheet" />
    </head>

    <body>
        <div gene-form></div>
        <script type="text/javascript" src='../../res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: '../../res/lib/layui-modules/js/'
            }).use(['gform', 'tool'], function() {
                window.$ = window.jQuery = layui.jquery;
                var radio = layui.gform.component('radio');
                /**
                 * 性别，继承单选输入组件
                 */
                var Sex = function() {};
                layui.tool.inherit(Sex, radio);
                $.extend(Sex.prototype, {
                    __initFieldComponent: function(options, __form, __row) {
                        var _that = this;
                        options = {
                            data: [{
                                name: 'sex',
                                value: '1',
                                title: '男',
                                checked: true
                            }, {
                                name: 'sex',
                                title: '女',
                                value: '0'
                            }]
                        };
                        radio.prototype.__initFieldComponent.call(_that, options, __form, __row);
                    }
                });
                layui.gform.component('sex', Sex);

                var gform = layui.gform.render({
                    rows: [{
                        label: '性别',
                        type: 'sex'
                    }]
                });
            });
        </script>
    </body>

</html>
```

