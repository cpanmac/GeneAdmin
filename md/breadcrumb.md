# 面包屑

## 第一部分 模块说明

模块名称：`breadcrumb`

扩展layui的面包屑，可以通过配置生成面包屑导航。

```html
<!DOCTYPE HTML>
<html>

    <head>
        <meta charset="utf-8">
        <link href="res/lib/layui/css/layui.css" rel="stylesheet" />
    </head>

    <body>
        <span gene-breadcrumb></span>
        <script type="text/javascript" src='res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: 'res/lib/layui-modules/js/'
            }).use(['breadcrumb'], function() {
                layui.breadcrumb.render({
                    data: ['导航一', '导航二', {
                        title: '百度',
                        cite: true,
                        href: "http://www.baidu.com"
                    }]
                });
            });
        </script>
    </body>

</html>
```

## 第二部分 参数详情

我们可以通过`layui.breadcrumb.render()`方法渲染一个面包屑导航，并获得面包屑对象。render方法的参数如下：

### 2.1 elem

面包屑容器，默认选择`span[gene-breadcrumb]`。

### 2.2 filter

元素的`lay-filter=""`的值，默认会自动生成一个随机值。

### 2.3 separator

分隔符，默认为`/`。

### 2.4 data

面包屑的数据信息，数据结构为数组，如果仅仅是想简单的显示一个面包屑导航，而不需要添加链接等，则数据项可以直接为字符串，否则数据项为JSON对象，具体数据项参数如下：

- title {String} 导航标题
- href {String} 超链接的href属性值，默认为：`javascript:;`
- cite {Boolean} 是否使用`cite`标签包裹标题

## 第三部分 面包屑对象方法

### 3.1 getFilter()

获取面包屑元素的`lay-filter`值。

## 第四部分 示例

### 4.1 最简单的用法

```html
<!DOCTYPE HTML>
<html>

    <head>
        <meta charset="utf-8">
        <link href="res/lib/layui/css/layui.css" rel="stylesheet" />
    </head>

    <body>
        <span gene-breadcrumb></span>
        <script type="text/javascript" src='res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: 'res/lib/layui-modules/js/'
            }).use(['breadcrumb'], function() {
                layui.breadcrumb.render({
                    data: ['导航一', '导航二']
                });
            });
        </script>
    </body>

</html>
```

### 4.2 小导航

```html
<!DOCTYPE HTML>
<html>

    <head>
        <meta charset="utf-8">
        <link href="res/lib/layui/css/layui.css" rel="stylesheet" />
    </head>

    <body>
        <span gene-breadcrumb></span>
        <script type="text/javascript" src='res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: 'res/lib/layui-modules/js/'
            }).use(['breadcrumb'], function() {
                layui.breadcrumb.render({
                    separator: '|',
                    data: ['导航一', '导航二']
                });
            });
        </script>
    </body>

</html>
```

