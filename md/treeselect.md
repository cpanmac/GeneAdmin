# 树形选择

## 第一部分 模块说明

模块名称：`treeselect`

树形选择是输入框与`zTree`的结合体，可以方便的是现在下拉的树中选择合适的值。

```html
<!DOCTYPE HTML>
<html>

    <head>
        <meta charset="utf-8">
        <link href="res/lib/layui/css/layui.css" rel="stylesheet" />
    </head>

    <body>
        <form class="layui-form" action="">
            <div class="layui-form-item">
                <label class="layui-form-label">多选下拉</label>
                <div class="layui-input-block layui-input-treeselect">
                    <input type="text" autocomplete="off" class="layui-input">
                </div>
            </div>
        </form>
        <script type="text/javascript" src='res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: 'res/lib/layui-modules/js/'
            }).use(['form', 'treeselect'], function() {
                var form = layui.form,
                    treeselect = layui.treeselect;
                layui.ztree.skin('default');
                treeselect.render({
                    elem: '.layui-input',
                    treeData: [{
                        id: 1,
                        name: '父节点一'
                    }, {
                        id: 2,
                        name: '父节点二',
                        open: true,
                        isParent: true,
                        children: [{
                            id: 3,
                            name: '子节点一'
                        }, {
                            id: 4,
                            name: '子节点二',
                            isParent: true,
                            children: [{
                                id: 5,
                                name: '子节点2.1'
                            }]
                        }]
                    }]
                });
            });
        </script>
    </body>

</html>
```

## 第二部分 参数详情

我们可以通过`layui.treeselect.render()`方法渲染树形选择。render方法的参数如下：

### 2.1 elem

需要渲染的输入框选择器。

### 2.2 treeData 

`zTree`树的数据参数。

> 1.0.3将`data`参数更名为`treeData`，现在的`data`参数为zTree的setting中的data参数。

### 2.3 separator 

值的分隔符，默认为：`,`。如果允许选择多值，则会根据该参数拆分聚合后的值字符串。

### 2.4 parentCheck 

父节点是否允许选择，默认：`false`。

### 2.5 async

等价于zTree的async参数。详情参见zTree官方文档。

### 2.6 data

等价于zTree的data参数。详情参见zTree官方文档。需要注意的是仅有如下参数有效：

- key
- simpleData

### 2.7 view

等价于zTree的view 参数。详情参见zTree官方文档。需要注意的是仅有如下参数有效：

- expandSpeed
- fontCss
- nameIsHTML
- selectedMulti
- showIcon
- showLine
- showTitle

### 2.8 callback

等价于zTree的callback参数。详情参见zTree官方文档。需要注意的是仅有如下参数有效：

- beforeCheck
- beforeAsync
- onAsyncError
- onAsyncSuccess

## 第三部分 管理端对象方法

### 3.1 render()

依据设置的值重新渲染。