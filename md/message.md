# 消息

## 第一部分 模块说明

模块名称：`message`

简易的消息提醒。

```html
<!DOCTYPE HTML>
<html>

    <head>
        <meta charset="utf-8">
        <link href="res/lib/layui/css/layui.css" rel="stylesheet" />
    </head>

    <body>
        <span gene-breadcrumb></span>
        <script type="text/javascript" src='res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: 'res/lib/layui-modules/js/'
            }).use(['message'], function() {
                layui.message.show('我是一条消息');
            });
        </script>
    </body>

</html>
```

## 第二部分 消息方法

消息的使用比较简单，引入消息模块后直接通过`layui.message.show()`方法就可以显示一条消息。

### 2.1 show()

如果只是想简单的显示一个消息，只需要传递一个字符串就可以了，如果想要更多的控制，详见下面的参数：

- skin {String} 皮肤，默认：`default`，可用值：`red`、`orange`、`cyan`、`blue`、`black`、`default`
- color {String} 自定义颜色，优先级高于`skin`
- msg {String} 消息内容，默认：`提示信息!`
- autoClose {Boolean} 是否自动关闭，默认：`true`
- timeout {Number} 自动关闭时间，默认：`3000`

## 第三部分 示例

### 3.1 红色消息

```html
<!DOCTYPE HTML>
<html>

    <head>
        <meta charset="utf-8">
        <link href="res/lib/layui/css/layui.css" rel="stylesheet" />
    </head>

    <body>
        <span gene-breadcrumb></span>
        <script type="text/javascript" src='res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: 'res/lib/layui-modules/js/'
            }).use(['message'], function() {
                layui.message.show({
                    msg: '我是一条消息',
                    skin: 'red'
                });
            });
        </script>
    </body>

</html>
```



