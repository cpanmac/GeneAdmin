# ztree

## 第一部分 模块说明

模块名称：`ztree`

该模块封装了`ztree`，版本号为：`3.5.35`，可以通过`layui.ztree.v`查看，使用详情请参见[http://treejs.cn/](http://treejs.cn/)。