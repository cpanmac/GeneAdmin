# 数据缓存

## 第一部分 模块说明

模块名称：`cache`

数据缓存模块如名称一样用于缓存我们在开发中的一些数据，该模块按需选择。

```
           entry             entry             entry             entry        
           ______            ______            ______            ______       
          | head |.newer => |      |.newer => |      |.newer => | tail |      
.oldest = |  A   |          |  B   |          |  C   |          |  D   | = .newest
          |______| <= older.|______| <= older.|______| <= older.|______|      
                                                                             
       removed  <--  <--  <--  <--  <--  <--  <--  <--  <--  <--  <--  added
```

## 第二部分 参数详情

我们可以通过`layui.cache.newCache()`方法生成一个数据缓存对象。newCache方法的参数如下：

### 2.1 maxLength

缓存的最大数据长度。

## 第三部分 数据缓存对象方法

### 3.1 put(key, value)

添加缓存，参数如下：

- key {String} 缓存键
- value {Object} 缓存值

### 3.2 get(key)

获得缓存数据，参数如下：

- key {String} 缓存键

## 第四部分 示例

```html
<!DOCTYPE HTML>
<html>

    <head>
        <meta charset="utf-8">
        <link href="res/lib/layui/css/layui.css" rel="stylesheet" />
    </head>

    <body>
        <script type="text/javascript" src='res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: 'res/lib/layui-modules/js/'
            }).use(['cache'], function() {
                var cache = layui.cache.newCache(10);
                cache.put('name', 'GeneAdmin');
                cache.put('author', 'gene.jiang');
                console.log(cache.get('name'));
                console.log(cache.get('author'));
            });
        </script>
    </body>

</html>
```

