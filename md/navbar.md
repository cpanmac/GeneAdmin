# 导航

## 第一部分 模块说明

模块名称：`navbar`

扩展layui的垂直/侧边导航，可以通过配置生成垂直/侧边导航。

```html
<!DOCTYPE HTML>
<html>

    <head>
        <meta charset="utf-8">
        <link href="res/lib/layui/css/layui.css" rel="stylesheet" />
    </head>

    <body>
        <ul gene-navbar></ul>
        <script type="text/javascript" src='res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: 'res/lib/layui-modules/js/'
            }).use(['navbar'], function() {
                layui.navbar.render({
                    data: [{
                        title: ' 产品'
                    }, {
                        title: ' 解决方案'
                    }]
                });
            });
        </script>
    </body>

</html>
```

## 第二部分 参数详情

我们可以通过`layui.navbar.render()`方法渲染一个导航，并获得导航对象。render方法的参数如下：

### 2.1 elem

导航容器，默认选择`ul[gene-navbar]`。

### 2.2 filter

元素的`lay-filter`的值，默认会自动生成一个随机值。

### 2.3 showTip

鼠标移至导航项是否显示tip，提示信息为导航项文本，默认：`false`。

### 2.4 bg

导航背景色，水平导航支持的其它背景主题有：`cyan（藏青）`、`molv（墨绿）`、`blue（艳蓝）`，垂直导航支持的其它背景主题有：`cyan（藏青）`。

### 2.5 navTree

是否为垂直导航，默认为：`false`。

### 2.6 navSide

是否为侧边导航，默认为：`false`。

### 2.7 beforeClick

导航项单击之前回调方法，如果返回`false`，则会阻止`click`方法执行。该方法接收两个参数，如下：

- navItem {jQuery} 导航项的jQuery对象
- event {String} 导航项的`event`参数值

### 2.8 click

导航项单击回调方法。该方法接收两个参数，如下：

- navItem {jQuery} 导航项的jQuery对象
- event {String} 导航项的`event`参数值

### 2.9 data

导航的数据信息，数据结构为数组，具体数据项参数如下：

- event {String} 事件，单击事件回调时会传入该参数，用于区分导航项的单击事件，如果存在二级导航，该参数无效
- unselectt {Boolean} 是否允许选中，仅一级导航生效
- href {String} 导航项超链接的href属性值，该参数与`event`不应该同时存在，如果存在二级导航，该参数无效
- attr {JSON} 导航项的属性，`href`属性也可以在此处设置
- select {Boolean} 是否选中，如果存在子导航，该参数无效
- shrink {Boolean} 展开子菜单时，是否收缩兄弟节点已展开的子菜单
- itemed {Boolean} 是否展开，如果不存在子导航，该参数无效
- children [Arrar] 二级导航数据，参数支持一级导航的所有参数
- icon {String} 导航图标，仅支持layui内置图标，例如：`&#xe65c;`
- eIcon {String} 导航图标，当layui内置图标不满足实际需求，需要自定义图标，可以使用该参数，例如：`<i class="fa fa-bug" aria-hidden="true"></i>`

## 第三部分 导航对象方法

### 3.1 getFilter()

获取导航元素的`lay-filter=""`值。

## 第四部分 示例

### 4.1 创建一个普通的水平导航

```html
<!DOCTYPE HTML>
<html>

    <head>
        <meta charset="utf-8">
        <link href="res/lib/layui/css/layui.css" rel="stylesheet" />
    </head>

    <body>
        <ul gene-navbar></ul>
        <script type="text/javascript" src='res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: 'res/lib/layui-modules/js/'
            }).use(['navbar'], function() {
                layui.navbar.render({
                    data: [{
                        title: ' 产品',
                        children: [{
                            title: '产品一'
                        }]
                    }, {
                        title: ' 解决方案'
                    }],
                    click: function(navItem) {
                        alert(navItem.text());
                    }
                });
            });
        </script>
    </body>

</html>
```

### 4.2 垂直导航

```html
<!DOCTYPE HTML>
<html>

    <head>
        <meta charset="utf-8">
        <link href="res/lib/layui/css/layui.css" rel="stylesheet" />
    </head>

    <body>
        <ul gene-navbar></ul>
        <script type="text/javascript" src='res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: 'res/lib/layui-modules/js/'
            }).use(['navbar'], function() {
                layui.navbar.render({
                    navTree: true,
                    data: [{
                        title: ' 产品',
                        itemed: true,
                        children: [{
                            title: '产品一'
                        }]
                    }, {
                        title: ' 解决方案'
                    }],
                    click: function(navItem) {
                        alert(navItem.text());
                    }
                });
            });
        </script>
    </body>

</html>
```

### 4.3 侧边导航

```html
<!DOCTYPE HTML>
<html>

    <head>
        <meta charset="utf-8">
        <link href="res/lib/layui/css/layui.css" rel="stylesheet" />
    </head>

    <body>
        <ul gene-navbar></ul>
        <script type="text/javascript" src='res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: 'res/lib/layui-modules/js/'
            }).use(['navbar'], function() {
                layui.navbar.render({
                    navTree: true,
                    navSide: true,
                    data: [{
                        title: ' 产品',
                        itemed: true,
                        children: [{
                            title: '产品一'
                        }]
                    }, {
                        title: ' 解决方案'
                    }],
                    click: function(navItem) {
                        alert(navItem.text());
                    }
                });
            });
        </script>
    </body>

</html>
```

