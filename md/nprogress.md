# 加载进度

## 第一部分 模块说明

模块名称：`nprogress`

该模块封装了`nprogress`，版本号为：`0.2.2`，可以通过`layui.nprogress.v`查看，使用详情请参见[http://ricostacruz.com/nprogress](http://ricostacruz.com/nprogress)。