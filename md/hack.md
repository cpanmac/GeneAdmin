# Hack

## 第一部分 模块说明

模块名称：`hack`

为了兼容部分浏览器，该模块修复了部分底层方法，并对一些低等对象的高频操作进行了扩展。

## 第二部分 修复及扩展底层方法

### 2.1 String

#### 2.1.1 trim()

修复字符串`trim`方法，移除字符串首尾空白字符。

示例：

```js
"   jianggujin   ".trim();//jianggujin
```

#### 2.1.2 format(args)

为字符串增加格式化字符串方法。

- `args` {Array|JSON} 格式化参数

示例：

```js
'hello,{0}'.format('jianggujin');          //'hello,jianggujin'
'hello,{name}'.format({name:'jianggujin'});//'hello,jianggujin'
```

#### 2.1.3 startsWith(str)

修复字符串`startsWith`方法，判断字符串是否以指定字符串开头。

示例：

```js
"jianggujin".startsWith("jiang");//true
```

#### 2.1.4 endsWith(str)

修复字符串`endsWith`方法，判断字符串是否以指定字符串结尾。

示例：

```js
"jianggujin".endsWith("jin");//true
```

### 2.2 Object

#### 2.2.1 create()

修复Object的`create`方法，用指定的原型对象及其属性去创建一个新的对象。

示例：

```js
Object.create(null);
```

#### 2.2.2 keys()

修复Object的`keys`方法，会返回一个由一个给定**对象**的自身可枚举属性组成的**数组**，数组中属性名的排列顺序和使用 `for...in`循环遍历该对象时返回的顺序一致 （两者的主要区别是 一个 for-in 循环还会枚举其原型链上的属性）。

示例：

```js
Object.keys(["jiang", "gu", "jin"]);//["0", "1", "2"]
```

### 2.3 Function

#### 2.3.1 bind()

修复IE10以下不支持Function的`bind`方法。创建一个新的函数, 当被调用时，将其this关键字设置为提供的值，在调用新函数时，在任何提供之前提供一个给定的参数序列。

### 2.4 Array

#### 2.4.1 isArray()

修复Array的`isArray`方法。用于确定传递的值是否是一个 `Array`。

示例：

```js
Array.isArray([1, 2, 3]); //true
Array.isArray({foo: 123});// false
```

#### 2.4.2 indexOf(item, index)

定位操作，返回数组中第一个等于给定参数的元素的索引值。如果不存在，则返回-1。

示例：

```js
let a = [2, 9, 7, 8, 9];
a.indexOf(2); // 0
a.indexOf(6); // -1
a.indexOf(7); // 2
a.indexOf(8); // 3
a.indexOf(9); // 1

if (a.indexOf(3) === -1) {
  // element doesn't exist in array
}
```

#### 2.4.3 lastIndexOf(item, index)

定位操作，同`indexOf`，不过是从后遍历。如果不存在则返回 -1。

示例：

```js
var array = [2, 5, 9, 2];
var index = array.lastIndexOf(2);
// index is 3
index = array.lastIndexOf(7);
// index is -1
index = array.lastIndexOf(2, 3);
// index is 3
index = array.lastIndexOf(2, 2);
// index is 0
index = array.lastIndexOf(2, -2);
// index is 0
index = array.lastIndexOf(2, -1);
// index is 3
```

#### 2.4.4 forEach(fn)

迭代操作，将数组的元素挨个儿传入一个函数中执行。

示例：

```js
['a', 'b', 'c'].forEach(function(element) {
    console.log(element);
});
```

####2.4.5 filter(fn)

迭代类 在数组中的每个项上运行一个函数，如果此函数的值为真，则此元素作为新数组的元素收集起来，并返回新数组。

示例：

```js
[1, 2, 3, 4, 5].filter(function(value) {
    return value > 3;
});//[4, 5]
```

#### 2.4.6 map(fn)

收集操作，将数组的元素挨个儿传入一个函数中执行，然后把它们的返回值组成一个新数组返回。

示例：

```js
[1, 2, 3, 4, 5].map(function(value) {
    return value * 2;
});//[2, 4, 6, 8, 10]
```

#### 2.4.7 some(fn)

只要数组中有一个元素满足条件（放进给定函数返回true），那么它就返回true。

示例：

```js
[1, 2, 3, 4, 5].some(function(value) {
    return value % 2 == 0;
});//true
```

#### 2.4.8 every(fn)

只有数组中的元素都满足条件（放进给定函数返回true），它才返回true。

示例：

```js
[1, 2, 3, 4, 5].every(function(value) {
    return value % 2 == 0;
});//false
```

#### 2.4.9 merge(arr)

合并数组。

示例：

```js
var arr = [1, 2];
arr.merge([3, 4]);
console.log(arr);//[1, 2, 3, 4]
```

#### 2.4.10 ensure(obj)

确保数组中不存在元素时添加

示例：

```js
var arr = [1, 2];
arr.ensure(3);
arr.ensure(1);
console.log(arr);//[1, 2, 3]
```

#### 2.4.11 removeAt(index)

移除数组指定索引处元素，返回布尔表示成功与否

示例：

```js
var arr = [1, 2, 3];
console.log(arr.removeAt(2));//true
console.log(arr);            //[1, 2]
```

#### 2.4.12 remove(obj)

移除数组中第一个匹配传参的那个元素，返回布尔表示成功与否

示例：

```js
var arr = [1, 2, 3];
console.log(arr.remove(2));//true
console.log(arr);          //[1, 3]
```
