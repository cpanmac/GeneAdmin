# 标签输入框

## 第一部分 模块说明

模块名称：`inputtags

```html
<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <title></title>
        <link href="res/lib/layui/css/layui.css" rel="stylesheet" />
    </head>

    <body>
        <form class="layui-form" action="">
            <div class="layui-form-item">
                <label class="layui-form-label">标签输入</label>
                <div class="layui-input-block">
                    <input type="text" autocomplete="off" class="layui-input" value="12,45">
                </div>
            </div>
        </form>
        <script type="text/javascript" src='../../res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: 'res/lib/layui-modules/js/'
            }).use(['form', 'inputtags'], function() {
                var inputtags = layui.inputtags;
                inputtags.render({
                    elem: '.layui-input'
                })
            });
        </script>
    </body>

</html>
```

## 第二部分 参数详情

我们可以通过`layui.inputtags.render()`方法渲染树形选择。render方法的参数如下：

### 2.1 elem

需要渲染的输入框选择器。

### 2.2 separator 

值的分隔符，默认为：`,`。如果允许选择多值，则会根据该参数拆分聚合后的值字符串。

### 2.3 placeholder

输入框的`placeholder`属性，默认：`回车生成标签`。

## 第三部分 管理端对象方法

### 3.1 render()

依据设置的值重新渲染。