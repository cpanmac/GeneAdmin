# 进度条

## 第一部分 模块说明

模块名称：`progress`

扩展layui的进度条，可以通过配置生成进度条，并可以通过返回的进度条对象直接操作进度。

```html
<!DOCTYPE HTML>
<html>

    <head>
        <meta charset="utf-8">
        <link href="res/lib/layui/css/layui.css" rel="stylesheet" />
    </head>

    <body>
        <div gene-progress></div>
        <script type="text/javascript" src='res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: 'res/lib/layui-modules/js/'
            }).use(['progress'], function() {
                layui.progress.render({
                    percent: '80%'
                });
            });
        </script>
    </body>

</html>
```

## 第二部分 参数详情

我们可以通过`layui.progress.render()`方法渲染一个进度条，并获得进度条对象。render方法的参数如下：

### 2.1 elem

进度条容器，默认选择`div[gene-progress]`。

### 2.2 filter

元素的`lay-filter=""`的值，默认会自动生成一个随机值。

### 2.3 showPercent

开启进度比的文本显示，默认：`false`。

### 2.4 big

是否为大尺寸的进度条风格。默认风格的进度条的百分比如果开启，会在右上角显示，而大号进度条则会在内部显示。默认：`false`。

### 2.5 percent

进度条的初始百分比。支持：普通数字、百分数、分数。

### 2.6 bg

进度条的背景色。可用值为：`red`、`orange`、`green`、`cyan`、`blu`e、`black`、`gray`，如果默认的背景色无法满需求，可以新增样式，样式名为：`layui-bg-*`，然后将该参数配置为`*`号部分即可。

## 第三部分 进度条对象方法

### 3.1 getFilter()

获取进度条元素的`lay-filter`值。

### 3.2 progress(percent)

设置进度条的进度，参数如下：

- percent {String} 进度条的百分比。支持：普通数字、百分数、分数。

## 第四部分 示例

### 4.1 模拟loading

```html
<!DOCTYPE HTML>
<html>

    <head>
        <meta charset="utf-8">
        <link href="res/lib/layui/css/layui.css" rel="stylesheet" />
    </head>

    <body>
        <div gene-progress></div>
        <script type="text/javascript" src='res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: 'res/lib/layui-modules/js/'
            }).use(['progress'], function() {
                var p = layui.progress.render({
                    showPercent: true,
                    big: true
                });
                //模拟loading
                var n = 0,
                    timer = setInterval(function() {
                        n = n + Math.random() * 10 | 0;
                        if(n > 100) {
                            n = 100;
                            clearInterval(timer);
                        }
                        p.progress(n + '%');
                    }, 300 + Math.random() * 1000);
            });
        </script>
    </body>

</html>
```

