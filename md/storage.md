# 数据存储

## 第一部分 模块说明

模块名称：`storage`

数据存储模块主要解决客户端数据存储的问题，比如表单数据缓存等，通过该模块您可以选择数据的生命周期是会话范围还是长期存在的，会优先使用`sessionStorage`或`sessionStorage`，如果运行在低版本的浏览器，不支持这两个对象，那么模块会选用Cookie进行数据存储。

> 注意：多个数据存储对象，同名数据会相互冲突，使用时应该避免数据键重复

## 第二部分 参数详情

我们可以通过layui.storage.newStorage()方法生成一个数据存储对象。newStorage方法的参数如下：

### 2.1 session

数据的生命周期是否为会话范围。默认为：`true`。

## 第三部分 数据存储对象方法

#### 2.1.1 getItem(name)

获得指定的数据，参数如下：

- name {String} 数据键

#### 2.1.2 setItem(name， value)

添加数据，参数如下：

- name {String} 数据键
- value {value}  数据值

#### 2.1.3 removeItem(name)

移除指定的数据，参数如下：

- name {String} 数据键

## 第四部分 示例

```html
<!DOCTYPE HTML>
<html>

    <head>
        <meta charset="utf-8">
        <link href="res/lib/layui/css/layui.css" rel="stylesheet" /> </head>

    <body>
        <script type="text/javascript" src='res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: 'res/lib/layui-modules/js/'
            }).use(['storage'], function() {
                var storage = layui.storage.newStorage();
                newStorage.setItem('name', 'GeneAdmin');
                newStorage.setItem('author', 'gene.jiang');
                console.log(newStorage.getItem('name'));
                console.log(newStorage.getItem('author'));
            });
        </script>
    </body>

</html>
```