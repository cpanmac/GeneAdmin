# 加载进度

## 第一部分 模块说明

模块名称：`placeholder`

该模块封装了`placeholder`，版本号为：`2.3.1`，可以通过`layui.placeholder.v`查看，使用详情请参见[https://github.com/mathiasbynens/jquery-placeholder](https://github.com/mathiasbynens/jquery-placeholder)。您可以直接通过`$.placeholder()`方法使用，也可以通过本模块提供的`layui.placeholder.placeholder(elem, options)`方法使用，该方法共有两个参数，第一个参数为要使用插件的文本域，第二个参数和原生的方法参数一致。