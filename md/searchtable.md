# 搜索表单表格

## 第一部分 模块说明

模块名称：`searchtable`

该模块是`search`模块与`table`模块的整合，如果一个页面仅做数据展示或者含有一些简单的操作，那么可以直接使用该模块。

```html
<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <link href="../../res/lib/layui/css/layui.css" rel="stylesheet" />
    </head>

    <body>
        <div gene-search-table></div>
        <script type="text/html" id="dataTableBar">
            <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
        </script>
        <script type="text/javascript" src='../../res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: '../../res/lib/layui-modules/js/'
            }).use(['searchtable'], function() {
                window.searchtable = layui.searchtable.render({
                    search: {
                        btns: {
                            useGroup: true,
                            items: [{
                                text: 'text',
                                event: 'event',
                                icon: '&#xe608;'
                            }]
                        },
                        form: {
                            keyword: {}
                        }
                    },
                    table: {
                        id: 'dataTable',
                        url: 'data.json',
                        cols: [
                            [{
                                field: 'id',
                                title: '编号',
                                sort: true,
                                width: 80
                            }, {
                                field: 'areaName',
                                title: '区域名称',
                                sort: true,
                                width: 300,
                                edit: 'text'
                            }, {
                                title: '操作',
                                fixed: 'right',
                                width: 100,
                                align: 'center',
                                toolbar: '#dataTableBar'
                            }]
                        ],
                        page: true,
                        even: true
                    },
                    callback: {
                        onClick: function() {
                            console.log(arguments);
                        },
                        onSearch: function(multivalue) {
                            console.log(multivalue.toMap());
                        },
                        onDone: function() {
                            console.log(arguments);
                        },
                        onTool: function() {
                            console.log(arguments);
                        }
                    },
                    tool: {
                        del: function() {
                            console.log('del');
                        }
                    }
                });
            });
        </script>
    </body>

</html>
```

## 第二部分 参数详情

我们可以通过`layui.searchtable.render()`方法渲染一个搜索表单，并获得搜索表单表格对象。render方法的参数如下：

### 2.1 elem

搜索表单表格容器，默认选择`div[gene-search-table]`。

### 2.2 search

`search`模块的配置，该参数的可用参数值为`search`模块除`callback`参数以外的所有参数

### 2.3 table

`table`模块的配置，该参数的可用参数值为`table`模块除`elem`参数以外的所有参数

### 2.4 callback

搜索表单表格的回调函数，可用参数如下：

- onClick {Function} 工具栏按钮单机回调，可用参数如下：
  - btn {jQuery} 工具栏按钮的jQuery对象
  - event {String} 配置的`event`参数值
  - e {Event} 单击事件
- onSearch {Function} 触发搜索的回调，可用参数如下：
  - multivalue {Multivalue} 表单数据

  > 需要注意的是该函数如果返回`false`，则会阻止默认的处理

- onDone {Function} 数据渲染完的回调，同`layui`的`done`参数
- onCheckbox {Function} 监听复选框选择回调，同`layui`的复选框选择回调
- onEdit {Function} 监听单元格编辑回调，同`layui`的单元格编辑回调
- onSort {Function} 监听排序切换回调，同`layui`的排序切换回调
- onRow {Function} 监听行单双击事件回调，同`layui`的行单双击事件回调
- onTool {Function} 监听工具条点击回调，同`layui`的工具条点击回调

### 2.5 tool

工具条点击回调，该参数是对`onTool`参数的扩展，该配置与`onTool`的区别在于`onTool`是处理所有的工具条点击回调，如果我们希望针对不同的工具条点击事件设置不同的回调函数，则可以使用本参数，参数的键值为工具条事件值。

## 第三部分 搜索表单表格对象方法

### 3.1 getSearch  ()

获得`search`对象。

### 3.2 getTable  ()

获得`table`对象。

### 3.3 getTableFilter ()

获得表格的`filter`参数。