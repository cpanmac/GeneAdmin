# 选项卡

## 第一部分 模块说明

模块名称：`tab`

扩展layui的选项卡，可以通过配置生成选项卡。

```html
<!DOCTYPE HTML>
<html>

    <head>
        <meta charset="utf-8">
        <link href="res/lib/layui/css/layui.css" rel="stylesheet" />
        <style type="text/css">
            html,
            body {
                height: 100%;
            }
        </style>
    </head>

    <body>
        <div gene-tab style="width: 100%;height: 100%;"></div>
        <script type="text/javascript" src='res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: '../../res/lib/layui-modules/js/'
            }).use(['tab'], function() {
                var tab = layui.tab.render();
                tab.tabAdd({
                    url: 'tab.html'
                });
            });
        </script>
    </body>

</html>
```

## 第二部分 参数详情

我们可以通过`layui.tab.render()`方法渲染一个选项卡，并获得选项卡对象。render方法的参数如下：

### 2.1 elem

选项卡容器，默认选择`div[gene-tab]`。

### 2.2 filter

元素的`lay-filter=""`的值，默认会自动生成一个随机值。

### 2.3 tabSwitch

选项卡选择回调函数，该函数有三个参数，如下：

- tabItem {jQuery} 选项卡jQuery对象
- layId {String} 选项卡的`lay-id`属性值
- tabIndex {Number} 选项卡的索引

### 2.4 frameHeightRefresh

`iframe`框架高度刷新时间，默认：500。仅在`Safari`浏览器中生效，实际测试的时候发现当多个iframe共存的时候，Safari浏览器会出现无法通过鼠标滚轮操作页面上下滚动，所以在Safari浏览器中通过不停地监听iframe内容高度是否变化来设置iframe的高度。

## 第三部分 选项卡对象方法

### 3.1 getFilter()

获取选项卡元素的`lay-filter`值。

### 3.2 getLayId()

获取当前选中的选项卡元素的`lay-id`值。

### 3.3 close()

关闭当前选中的选项卡，添加选项卡的时候必须是允许关闭的。

### 3.4 refresh()

刷新当前选中的选项卡。

### 3.5 closeOther()

关闭非当前选中的允许关闭的选项卡。

### 3.6 closeAll()

关闭所有允许关闭的选项卡。

### 3.7 tabCount()

获取选项卡个数。

### 3.8 getLayId(layId)

获取指定选项卡的jQuery对象，参数如下：

- layId {String} 选项卡的`lay-id`属性

### 3.9 tabExists(layId)

判断指定选项卡是否存在，参数如下：

- layId {String} 选项卡的`lay-id`属性

### 3.10 tabDelete(tabDelete, sync)

删除指定选项卡，参数如下：

- layId {String} 选项卡的`lay-id`属性
- sync {Boolean} 是否同步工具栏状态

### 3.11 tabDeleteIndex(index)

删除指定索引位置的选项卡，参数如下：

- index {Number} 选项卡的索引

### 3.12 tabChange(layId)

显示指定选项卡，参数如下：

- layId {String} 选项卡的`lay-id`属性

### 3.13 tabChangeIndex(index)

显示指定索引位置选项卡，参数如下：

- index {Number} 选项卡的索引

### 3.14 tabAdd(options)

添加选项卡，该方法会返回新添加的选项卡的`lay-id`属性值，`options`为JSON数据，参数如下：

- attr {JSON} 选项卡元素的属性
- title {String} 选项卡标题
- icon {String} 选项卡图标，仅支持layui内置图标，例如：`&#xe65c;`
- eIcon {String} 选项卡图标，当layui内置图标不满足实际需求，需要自定义图标，可以使用该参数，例如：`<i class="fa fa-bug" aria-hidden="true"></i>`
- url {String} 选项卡地址
- id {String} 选项卡`lay-id`属性
- openWait {Boolean} 打开选项卡的时候是否等待，默认：`true`
- canClose {Boolean} 选项卡是否可以关闭，默认：`true`

