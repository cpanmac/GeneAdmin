# 请求处理

## 第一部分 模块说明

模块名称：`multivalue`

与其说这是一个模块，不如说这是一种数据类型更为贴切，`multivalue`提供了一个键对应多个值的能力。

## 第二部分 参数详情

我们可以通过`layui.multivalue.newMultiValue()`方法获得多值数据类型对象。该方法不需要任何参数。

## 第三部分 多值数据类型对象方法

### 3.1 put(key, value)

添加数据，参数如下：

- key {String} 数据键
- value {Object} 数据值

### 3.2 remove(key)

移除数据，参数如下：

- key {String} 数据键

### 3.3 get(key)

获得数据，参数如下：

- key {String} 数据键

### 3.4 getFirst(key)

获得指定键值对应的数据的第一条数据记录，参数如下：

- key {String} 数据键 

## 第四部分 示例

```html
<!DOCTYPE HTML>
<html>

    <head>
        <meta charset="utf-8">
        <link href="res/lib/layui/css/layui.css" rel="stylesheet" />
    </head>

    <body>
        <script type="text/javascript" src='res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: 'res/lib/layui-modules/js/'
            }).use(['multivalue'], function() {
                var multivalue = layui.multivalue.newMultiValue();
                multivalue.put('name', 'GeneAdmin');
                multivalue.put('name', 'GeneAdmin');
                multivalue.put('author', 'gene.jiang');
                console.log(multivalue.get('name'));
                console.log(multivalue.getFirst('name'));
                console.log(multivalue.get('author'));
            });
        </script>
    </body>

</html>
```