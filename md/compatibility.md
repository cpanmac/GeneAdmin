# 浏览器兼容

## 第一部分 CSS兼容

### 1.1 响应式布局

IE9以下浏览器不支持响应式布局，GeneAdmin提供了相关的兼容js文件，我们只需要在head中引入就可以。

```html
<!--[if lt IE 9]>
<script type="text/javascript" src='res/lib/html5shiv/html5shiv.min.js'></script>
<script type="text/javascript" src='res/lib/respond/respond.min.js'></script>
<![endif]-->
```

### 1.2 background-size

CSS3中新增了`background-size`属性，IE9+、Firefox 4+、Opera、Chrome 以及 Safari 5+ 支持 background-size 属性，在低版本IE中并不支持，有的时候我们希望把背景图像扩展至足够大，以使背景图像完全覆盖背景区域。我们可以在相关元素的样式中添加`behavior`实现浏览器的兼容，如如DEMO中的登录页中的背景。

```css
body {
    background: #393D49 url("../img/demo-1-bg.jpg") no-repeat;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    background-size: cover;
    -ms-behavior: url("../../res/lib/background-size-polyfill/backgroundsize.min.htc");
    behavior: url("../../res/lib/background-size-polyfill/backgroundsize.min.htc");
    min-width: 500px;
}
```

## 第二部分 JS兼容

详见`hack`模块部分文档。

## 第三部分 通用head写法

```html
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
    <meta content="telephone=no" name="format-detection">
    <meta content="email=no" name="format-detection">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link href="favicon.ico" mce_href="favicon.ico" rel="Bookmark" type="image/x-icon" />
    <link href="favicon.ico" mce_href="favicon.ico" rel="icon" type="image/x-icon" />
    <link href="favicon.ico" mce_href="favicon.ico" rel="Shortcut Icon" type="image/x-icon" />
    <!-- Standard iPhone -->
    <link rel="apple-touch-icon" sizes="57x57" href="touch-icon-iphone-114.png" />
    <!-- Retina iPhone -->
    <link rel="apple-touch-icon" sizes="114x114" href="touch-icon-iphone-114.png" />
    <!-- Standard iPad -->
    <link rel="apple-touch-icon" sizes="72x72" href="touch-icon-ipad-144.png" />
    <!-- Retina iPad -->
    <link rel="apple-touch-icon" sizes="144x144" href="touch-icon-ipad-144.png" />
    <title>GeneAdmin</title>
    <link href="res/lib/layui/css/layui.css" rel="stylesheet" />
    <script type="text/javascript">
        if(self !== top && top.location.origin != self.location.origin) {
           top.location = self.location;
        }
    </script>
</head>
```

