# 顶部导航（一级导航）

## 第一部分 模块说明

模块名称：`topnav`

顶部导航用于生成后台页面的头部，一般不独立使用，在管理端模块中已经集成，如果想要独立使用，可参考下面的代码结构。顶部导航由四部分组成，分别为：图标、移动端图标、左侧导航、右侧导航，当然了，这四部分并不是一定都要存在的。

```html
<!DOCTYPE HTML>
<html>

    <head>
        <meta charset="utf-8">
        <link href="res/lib/layui/css/layui.css" rel="stylesheet" />
        <link href="res/lib/layui-modules/css/admin.css" rel="stylesheet" />
    </head>

    <body>
        <div class="layui-layout layui-layout-admin gene-layout-admin">
            <div class="layui-header" gene-top-nav></div>
        </div>
        <script type="text/javascript" src='res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: 'res/lib/layui-modules/js/'
            }).use(['topnav'], function() {
                layui.topnav.render({
                    logo: 'GENE ADMIN',
                    mLogo: 'G',
                    left: [{
                        title: ' 产品',
                        itemed: true,
                        children: [{
                            title: '产品一'
                        }]
                    }, {
                        title: ' 解决方案'
                    }]
                });
            });
        </script>
    </body>

</html>
```

## 第二部分 参数详情

我们可以通过`layui.topnav.render()`方法渲染一个顶部导航，并获得顶部导航对象。render方法的参数如下：

### 2.1 elem

导航容器，默认选择div`[gene-top-nav]`。

### 2.2 leftFilter

左侧导航元素的`lay-filter`的值，默认会自动生成一个随机值。

### 2.3 rightFilter

右侧导航元素的`lay-filter`的值，默认会自动生成一个随机值。

### 2.4 logo

图标内容，会使用`<div class="layui-logo"></div>`包裹。

### 2.5 mLogo

移动端图标内容，会使用`<div class="layui-logo gene-logo-mobile"></div>`包裹。

### 2.6 beforeClick

导航项单击之前回调方法，如果返回`false`，则会阻止`click`方法执行。该方法接收两个参数，如下：

- navItem {jQuery} 导航项的jQuery对象
- event {String} 导航项的`event`参数值

### 2.7 click

导航项单击回调方法。该方法接收两个参数，如下：

- navItem {jQuery} 导航项的jQuery对象
- event {String} 导航项的`event`参数值

### 2.8 left

左侧导航导航的数据信息，数据结构为数组，具体数据项参数如下：

- event {String} 事件，单击事件回调时会传入该参数，用于区分导航项的单击事件，如果存在二级导航，该参数无效

- href {String} 导航项超链接的href属性值，该参数与`event`不应该同时存在，如果存在二级导航，该参数无效

- attr {JSON} 导航项的属性，`href`属性也可以在此处设置

- select {Boolean} 是否选中，如果存在二级导航，该参数无效

- children [Arrar] 二级导航数据，参数支持一级导航的除`itemed `、`children `参数以外的所有参数

- icon {String} 导航图标，仅支持layui内置图标，例如：`&#xe65c;`

- eIcon {String} 导航图标，当layui内置图标不满足实际需求，需要自定义图标，可以使用该参数，例如：`<i class="fa fa-bug" aria-hidden="true"></i>`

## 2.9 right


右侧导航导航的数据信息，数据结构为数组，具体数据项参数如下：

- event {String} 事件，单击事件回调时会传入该参数，用于区分导航项的单击事件，如果存在二级导航，该参数无效

- href {String} 导航项超链接的href属性值，该参数与`event`不应该同时存在，如果存在二级导航，该参数无效

- attr {JSON} 导航项的属性，`href`属性也可以在此处设置

- select {Boolean} 是否选中，如果存在二级导航，该参数无效

- children [Arrar] 二级导航数据，参数支持一级导航的除`itemed `、`children `参数以外的所有参数

- icon {String} 导航图标，仅支持layui内置图标，例如：`&#xe65c;`

- eIcon {String} 导航图标，当layui内置图标不满足实际需求，需要自定义图标，可以使用该参数，例如：`<i class="fa fa-bug" aria-hidden="true"></i>`

## 第三部分 导航对象方法

### 3.1 getLeftFilter()

获取左侧导航元素的`lay-filter=""`值。

### 3.2 getRightFilter()

获取右侧导航元素的`lay-filter=""`值。

## 第四部分 示例

```html
<!DOCTYPE HTML>
<html>

    <head>
        <meta charset="utf-8">
        <link href="res/lib/layui/css/layui.css" rel="stylesheet" />
        <link href="res/lib/layui-modules/css/admin.css" rel="stylesheet" />
    </head>

    <body>
        <div class="layui-layout layui-layout-admin gene-layout-admin">
            <div class="layui-header" gene-top-nav></div>
        </div>
        <script type="text/javascript" src='res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: 'res/lib/layui-modules/js/'
            }).use(['topnav'], function() {
                layui.topnav.render({
                    logo: 'GENE ADMIN',
                    mLogo: 'G',
                    left: [{
                        title: ' 产品',
                        itemed: true,
                        children: [{
                            title: '产品一'
                        }]
                    }, {
                        title: ' 解决方案'
                    }],
                    right: [{
                        title: 'Gene',
                        children: [{
                            title: ' 个人中心',
                            icon: '&#xe612;'
                        }, {
                            title: ' 修改资料',
                            icon: '&#xe614;'
                        }]
                    }, {
                        title: '注销'
                    }],
                    click: function(navItem) {
                        alert(navItem.text());
                    }
                });
            });
        </script>
    </body>

</html>
```

