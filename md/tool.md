# tool

## 第一部分 模块说明

模块名称：`tool`

该模块提供一些常用方法实现。

## 第二部分 方法详情

### 2.1 load(type)

加载层，layui的加载层封装，默认添加`shade: [0.3, '#333']`，参数如下：

- type {Number} 类型，默认为：`0`

### 2.2 makeHashCode(prefix)

生成随机字符串。

- `prefix` {String} 字符串前缀，默认值为：`gene`

### 2.3 isNative(fn)

判断方法是否为底层方法。

- `fn` {Function} 待检测的方法

### 2.4 arrayEqual(arr1, arr2)

判断两个数组是否相等。

- `arr1` {Array} 第一个数组
- `arr2` {Array} 第二个数组

### 2.5 cookie(name, value, options)

读写Cookie值。

- `name` {String} cookie名称
- `value` {String} cookie值
- `options` {JSON} cookie选项

### 2.6 removeCookie(name)

删除Cookie。

- `name` {String} cookie名称

### 2.7 getExplore()

获取浏览器类型(`name`)和版本(`version`)。支持：`IE`、`EDGE`、`Firefox`、`Chrome`、`Opera`、`Safari`。

### 2.8 getOS()

获得操作系统类型。支持：`mac`、`windowsPhone`、`windows`、`linux`、`ios`、`android`。

### 2.9 device(key)

获得设备信息，该方法为`getExplore()`与`getOS()`的合集，并扩展了功能，可以验证是否为指定环境。

- `key` {String} 自定义标识

### 2.10 loadCSS(href, callback)

动态加载CSS样式。需要注意的是，如果`debug`为`true`，则该方法会在`href`后面添加参数名为`v`的时间戳。

- `href` {String} css路径
- `callback` {Function} 回调函数，该函数有两个参数，第一个参数为Boolean类型，表示是否加载成功。第二个参数同href

### 2.11 loadScript(src, callback)

动态加载js脚本。需要注意的是，如果`debug`为`true`，则该方法会在`src`后面添加参数名为`v`的时间戳。

- `src` {String} js路径
- `callback` {Function} 回调函数，该函数有两个参数，第一个参数为Boolean类型，表示是否加载成功。第二个参数同src

### 2.12 inherit(Child, Parent)

实现Child继承Parent中的方法。

- `Child` {Object} 子类
- `Parent` {Object} 父类

### 2.13 openBlank(attrs, params)

通过表单将参数提交到指定地址。

- `attrs` {JSON|String} 表单属性或action地址
- `params` {JSON} 请求参数

### 1.14 hyphen(target)

 转换为连字符线风格。

- `target` {String} 待转换字符串

### 2.15 camelize(target)

转换为驼峰风格。

- `target` {String} 待转换字符串

### 2.16 shadowCopy(destination, source)

属性拷贝，浅拷贝。

- `destination` {Object} 目标对象
- `source` {Object} 源对象