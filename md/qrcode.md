# qrcode

## 第一部分 模块说明

模块名称：`qrcode`

该模块封装了QRCode，使用详情请参见[https://github.com/davidshimjs/qrcodejs](https://github.com/davidshimjs/qrcodejs)