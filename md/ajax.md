# 异步请求

## 第一部分 模块说明

模块名称：`ajax`

扩展jQuery的ajax请求，为jQuery的ajax方法增加成功方法与错误方法的拦截方法，在原有ajax参数基础上新增beforeSuccess、beforeError、afterRemote三个方法，分别用于成功方法前置拦截、错误方法前置拦截、请求完成方法（不论成功失败，都会执行）。该模块主要用于对响应数据的预处理或者请求结束的清理工作。

引入ajax模块后，我们可以像平时使用jQuery的ajax方法一样，具体参数可参考：[http://api.jquery.com/jQuery.ajax/](http://api.jquery.com/jQuery.ajax/)

```js
$.ajax({
    url: 'data.json',
    success: function(data){
        console.log(data);
    }
});
```

除了传统的写法，我们也是可以使用ajax模块提供的`remote`方法，本质上该方法和`jQuery.ajax`是一样的。

```js
<script type="text/javascript">
    layui.config({
        base: 'res/lib/layui-modules/js/'
    }).use(['ajax'], function() {
        var ajax = layui.ajax;
        ajax.remote(({
            url: 'data.json',
            success: function(data){
                console.log(data);
            }
        });
    });
</script>
```

## 第二部分 参数详情

### 2.1 beforeSuccess

该方法会在请求成功之后，在执行`success`之前执行，参数与`success`方法基本相同，需要注意的是，使用`success`方法，第一个参数就是请求的返回值，但是`beforeSuccess`不同，该方法的第一个参数是一个JSON对象，我们需要通过该对象的`data`属性获得请求的数据，我们可以通过修改`data`的值来影响`success`方法获得的实际数据。如果该方法返回`false`，则会阻止`success`方法的执行，不管方法返回什么值都不会影响`afterRemote`方法的执行。

### 2.2 beforeError

该方法会在请求成功之后，在执行`error`之前执行，参数与`error`方法相同，如果该方法返回`false`，则会阻止`error`方法的执行，不管方法返回什么值都不会影响`afterRemote`方法的执行。

### 2.3 afterRemote 

只要请求结束，不管成功失败，都会执行`afterRemote`方法，该方法有四个参数：

- success {Boolean} 是否成功
- notIntercept {Boolean} 是否拦截，如果成功或失败对应的before方法返回false，则该参数为：false，否则为：true
- args {Array} 如果成功，则数组内容为success的参数，否则为error的参数
- xhr {XMLHttpRequest} XMLHttpRequest对象

## 第三部分 示例

```html
<!DOCTYPE HTML>
<html>

    <head>
        <meta charset="utf-8">
        <link href="res/lib/layui/css/layui.css" rel="stylesheet" />
    </head>

    <body>
        <script type="text/javascript" src='res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: 'res/lib/layui-modules/js/'
            }).use(['ajax'], function() {
                window.$ = window.jQuery = layui.jquery;
                $.ajax({
                    url: 'data.json',
                    beforeSuccess: function() {
                        console.log('beforeSuccess');
                        console.log(arguments);
                    },
                    success: function() {
                        console.log('success');
                        console.log(arguments);
                    },
                    beforeError: function() {
                        console.log('beforeError');
                        console.log(arguments);
                    },
                    error: function() {
                        console.log('error');
                        console.log(arguments);
                    },
                    afterRemote: function() {
                        console.log('afterRemote');
                        console.log(arguments);
                    }
                })
            });
        </script>
    </body>

</html>
```

