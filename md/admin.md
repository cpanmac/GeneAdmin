# 管理端

## 第一部分 模块说明

模块名称：`admin`

管理端是对`导航(navbar)`、`顶部导航(topnav)`以及`内容`的综合，使用管理端模块，我们很容易的创建出后台管理端的页面。

```html
<!DOCTYPE HTML>
<html>

    <head>
        <meta charset="utf-8">
        <link href="res/lib/layui/css/layui.css" rel="stylesheet" />
    </head>

    <body>
        <div gene-admin></div>
        <script type="text/javascript" src='res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: 'res/lib/layui-modules/js/'
            }).use(['admin'], function() {
                window.$ = window.jQuery = layui.jquery;
                var admin = layui.admin.render({
                    topnav: {
                        logo: 'GENE ADMIN',
                        mLogo: 'G',
                        left: [],
                        right: []
                    },
                    navbar: {
                    }
                });
            });
        </script>
    </body>

</html>
```

## 第二部分 参数详情

我们可以通过`layui.admin.render()`方法渲染一个管理端，并获得管理端对象。render方法的参数如下：

### 2.1 elem

管理端容器，默认选择`div[gene-admin]`。

### 2.2 collapse

左侧导航菜单收缩回调方法，该方法没有任何参数。

### 2.3 expand

左侧导航菜单展开回调方法，该方法没有任何参数。

### 2.4 footer

是否渲染管理端底部区域，通常我们会在该区域展示版权信息，如果该参数有值，则会自动构建`<div class="layui-footer"></div>`底部区域，并将参数值直接添加进去，如果不需要，则不需要配置该参数。

### 2.7 navbar

左侧导航参数，参数与**导航(navbar)模块**一致。如果不配置该参数，则不初始化左侧导航。

> 需要注意的是，在初始化导航的时候应该为每一个导航设置图标，当导航收缩的时候仅会显示图标，如果不配置该参数，应该禁用初始化折叠按钮，否则当导航收缩后显示会很诡异。

### 2.8 hasFold

是否初始化折叠按钮，该按钮用于收缩/展开左侧导航，只有当开启了左侧导航，该参数才生效，默认为：`true`。

> 从1.0.2版本开始，如果不开启折叠按钮，则不会加载

### 2.9 topnav

顶部导航参数，参数与**顶部导航(topnav)模块**一致。如果不配置该参数，则不初始化顶部导航。

### 2.10 content

> 1.0.2新增

中间区域内容，可以是任何能被`jQuery`选择的参数，会调用`html()`方法，将结果设置为中间区域内容。

### 2.11 src

> 1.0.2新增

中间区域内容，如果设置该参数，将会使用`iframe`加载页面内容，如果设置了`content`参数，则在初始化时本参数无效。

### 2.12 collapsed

> 1.0.1新增

是否收缩左侧导航，默认不收缩。

## 第三部分 管理端对象方法

### 3.1 isCollapsed()

判断当前左侧导航是否为收缩状态。

### 3.2 toogle()

收缩/展开左侧导航。

### 3.3 collapse()

收缩左侧导航。

### 3.4 expand()

展开左侧导航。

### 3.5 getNavbar()

获得初始化的左侧导航对象。

### 3.6 getTopnav()

获得初始化的顶部导航对象。

### 3.7 getFooter()

获得初始化的页脚容器的jQuery对象。

### 3.8 setSrc(url)

设置中间区域的`iframe`的加载地址，参数如下：

- url {String} `iframe`的`src`属性

### 3.9 setSrc2(url)

设置中间区域的`iframe`的加载地址，参数如下：

- url {String} `iframe`的`src`属性

> 需要注意的是，该方法会在设置之前先对`iframe`的`src`属性设置为`""`，防止某些情况，`iframe`无法正常加载

### 3.9 getSrc()

获得中间区域的`iframe`的加载地址。