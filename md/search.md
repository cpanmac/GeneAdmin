# 搜索表单

## 第一部分 模块说明

模块名称：`search`

该模块是对`gform`模块的增强，建议需要通过表单查询数据的时候可以使用本模块代替`gform`模块。

```html
<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <link href="../../res/lib/layui/css/layui.css" rel="stylesheet" />
    </head>

    <body>
        <div gene-search></div>
        <script type="text/javascript" src='../../res/lib/layui/layui.js'></script>
        <script type="text/javascript">
            layui.config({
                base: '../../res/lib/layui-modules/js/'
            }).use(['search'], function() {
                layui.search.render({
                    btns: {
                        useGroup: true,
                        items: [{
                            text: 'text',
                            event: 'event',
                            icon: '&#xe608;'
                        }]
                    },
                    form: {
                        keyword: {},
                        rows: [{
                            label: '选择框',
                            type: 'select',
                            name: 'sel1',
                            data: [{
                                text: '选项一',
                                value: '1'
                            }]
                        }]
                    },
                    callback: {
                        onClick: function() {
                            console.log(arguments);
                        },
                        onSearch: function(multivalue) {
                            console.log(multivalue.toMap());
                        }
                    }
                });
            });
        </script>
    </body>
</html>
```

## 第二部分 参数详情

我们可以通过`layui.search.render()`方法渲染一个搜索表单，并获得搜索表单对象。render方法的参数如下：

### 2.1 elem

搜索表单容器，默认选择`div[gene-search]`。

### 2.2 btns

工具栏按钮组，用户可以自定义工具栏按钮完成额外的工作。数据类型为：`JSON`，参数详情如下：

- useGroup {Boolean} 是否使用`layui-btn-group `样式
- item  {Array} 工具栏按钮配置，每一项可用参数如下：
  - text {String} 按钮文本
  - event {String} 事件，单击事件回调时会传入该参数
  - href {String} 工具栏按钮超链接的href属性值，该参数与`event`不应该同时存在
  - skin {String} 皮肤，可用值为：`primary`、`normal`、`warm`、`danger`、`disabled`
  - attr {JSON} 工具栏按钮的属性，`href`属性也可以在此处设置
  - icon {String} 按钮图标，仅支持layui内置图标，例如：`&#xe65c;`
  - eIcon {String} 按钮图标，当layui内置图标不满足实际需求，需要自定义图标，可以使用该参数，例如：`<i class="fa fa-bug" aria-hidden="true"></i>`

### 2.3 form

搜索表单的配置。数据类型为：`JSON`，参数详情如下：

- disableDefault {Boolean} 是否禁用默认的确定按钮，如果禁用则不渲染该部分，默认：`false`
- defaultText {String} 默认确定按钮的文本，默认值：`确定`
- keyword  {JSON} 搜索表单的关键词输入部分，配置该项后，搜索表单会渲染成单行模式，通过弹出层选择更多的搜索条件，不过不配置该参数，则渲染成默认的表单形式。可用参数如下：
  - name {String} 关键词输入项的`name`属性
  - placeholder {String} 关键词输入项的`placeholder`属性
  - icon {String} 关键词输入项图标，仅支持layui内置图标，例如：`&#xe65c;`
  - eIcon {String} 关键词输入项图标，当layui内置图标不满足实际需求，需要自定义图标，可以使用该参数，例如：`<i class="fa fa-bug" aria-hidden="true"></i>
  - moreText {String} 更多筛选条件的显示文本，默认：`更多筛选`，如果不配置`rows`参数，则不会渲染该部分
  - layout {String} 关键词输入项布局，默认为关键词输入项在左，工具栏按钮再由，如果想要更改顺序，则只需要将该值配置为：`r`
- rows {Array} 表单的每一行数据。参数与`gform`模块的`rows`参数相同

### 2.4 callback

搜索表单的回调函数，可用参数如下：

- onClick {Function} 工具栏按钮单机回调，可用参数如下：
  - btn {jQuery} 工具栏按钮的jQuery对象
  - event {String} 配置的`event`参数值
  - e {Event} 单击事件
- onSearch {Function} 触发搜索的回调，可用参数如下：
  - multivalue {Multivalue} 表单数据

## 第三部分 搜索表单对象方法

### 3.1 __showMore()

显示更多的表单弹层。

### 3.2 __hideMore()

隐藏更多的表单弹层。
