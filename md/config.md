# 环境配置

## 第一部分 模块说明

模块名称：`geneconfig`

该模块用于配置系统上下文路径、自定义模块路径。可以直接修改该文件，也可以在引入模块之前增加对应变量，模块会按照实际配置的值进行处理。

## 第二部分 参数详情

### 2.1 context

系统上下文路径，对应覆盖的变量为：`_context`，默认：空字符串

### 2.2 modules

自定义模块路径，对应覆盖的变量为：`_modules`，默认： `/res/lib/layui-modules`

## 第三部分 示例

### 3.1 直接修改文件

```js
/**
 * Name:geneconfig.js
 * Author:gene.jiang
 * E-mail:1013195469@qq.com
 * Website:http://www.jianggujin.com/
 * LICENSE:MIT
 * Description:配置文件
 */
layui.define(function(exports) {

    var geneconfig = {
        context: '', //系统上下文路径
        mudules: '/res/lib/layui-modules', //自定义模块路径
        getModulePath: function() {
            return this.context + this.mudules;
        }
    };

    exports('geneconfig', geneconfig);
});
```

### 3.2 使用覆盖变量

```html
<script type="text/javascript">var _context = '/GeneAdmin';</script>
<script type="text/javascript" src='res/lib/layui/layui.js'></script>
```

